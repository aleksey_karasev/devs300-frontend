import axios, { AxiosRequestConfig, AxiosResponse, RawAxiosRequestHeaders } from "axios";
import { ScheduleProps, Visit } from "../../Components/Schedule/Props";
import { VisitForDoctor } from "../../Components/VisitProcedure/Props";

let data = `{
    "periodName": "Период с 01.01.2024 по 02.01.2024",
    "days": [
      {
        "date": "01.01.2024",
        "visits": [
          {
            "timePeriod": "00:00 - 01:00",
            "patient": "Фёдоров Сергей Петрович",
            "doctor": "Алексеев Георгий Давыдович",
            "comment": "Пациент буйный",
            "visitId": 123
          },
          {
            "timePeriod": "01:00 - 02:00",
            "patient": null,
            "doctor": null,
            "comment": null,
            "visitId": 124
          },
          {
            "timePeriod": "03:00 - 04:00",
            "patient": null,
            "doctor": null,
            "comment": null,
            "visitId": 125
          },
          {
            "timePeriod": "04:00 - 05:00",
            "patient": "Владимир Иванович Буйко",
            "doctor": "Кантемир2 Фиолкатович Грызайло",
            "comment": null,
            "visitId": 126
          },
          {
            "timePeriod": "05:00 - 06:00",
            "patient": null,
            "doctor": null,
            "comment": null,
            "visitId": 127
          },
          {
            "timePeriod": "06:00 - 07:00",
            "patient": null,
            "doctor": null,
            "comment": null
          }
        ]
      },
      {
        "date": "02.01.2024",
        "visits": [
          {
            "timePeriod": "00:00 - 01:00",
            "patient": null,
            "doctor": null,
            "comment": null
          },
          {
            "timePeriod": "01:00 - 02:00",
            "patient": null,
            "doctor": null,
            "comment": null
          },
          {
            "timePeriod": "03:00 - 04:00",
            "patient": null,
            "doctor": null,
            "comment": null
          },
          {
            "timePeriod": "04:00 - 05:00",
            "patient": null,
            "doctor": null,
            "comment": null
          },
          {
            "timePeriod": "05:00 - 06:00",
            "patient": null,
            "doctor": null,
            "comment": null
          },
          {
            "timePeriod": "06:00 - 07:00",
            "patient": null,
            "doctor": null,
            "comment": null
          }
        ]
      }
      ,
      {
        "date": "02.01.2024",
        "visits": [
          {
            "timePeriod": "00:00 - 01:00",
            "patient": null,
            "doctor": null,
            "comment": null
          },
          {
            "timePeriod": "01:00 - 02:00",
            "patient": null,
            "doctor": null,
            "comment": null
          },
          {
            "timePeriod": "03:00 - 04:00",
            "patient": null,
            "doctor": null,
            "comment": null
          },
          {
            "timePeriod": "04:00 - 05:00",
            "patient": null,
            "doctor": null,
            "comment": null
          },
          {
            "timePeriod": "05:00 - 06:00",
            "patient": null,
            "doctor": null,
            "comment": null
          },
          {
            "timePeriod": "06:00 - 07:00",
            "patient": null,
            "doctor": null,
            "comment": null
          }
        ]
      }
    ]
  }`;

  const client = axios.create({
    baseURL: 'https://localhost:7126/api/v1/',
  });
  
  async function getScheduleFromBackend()  {
    try {      
      const response: AxiosResponse = await client.get(`/Schedule/simple?begin=2024-01-01&end=2024-01-03`);
      console.log(response.status);
      console.log(response.data);   
      return response.data;
    } catch(err) {
      console.log(err);
      alert('error login on backend' + err);
    } 
    
  };

export async function GetSchedule(beginSchedule: string)  {  
    //let props: ScheduleProps = JSON.parse(data);   
   let schedule: ScheduleProps = {};
  try {
    // const { data } = await axios.get<ScheduleProps>('https://localhost:7126/api/v1/Schedule/simple?begin=2024-01-08&end=2024-01-15',
    //                                                 {
    //                                                   headers: {
    //                                                       'Authorization': "Bearer " + localStorage.getItem('id_token')                                                          
    //                                                   }
    //                                                 });
    const { data } = await axios.get<ScheduleProps>('https://localhost:7126/api/v1/Schedule/simple?begin='+beginSchedule,
                                                    {
                                                      headers: {
                                                          'Authorization': "Bearer " + localStorage.getItem('id_token')                                                          
                                                      }
                                                    });                                                    
   // const { data } = await axios.get<ScheduleProps>('https://localhost:7126/api/v1/Schedule/simple?begin=2024-01-08&end=2024-01-15');
    schedule = data;
  } catch (error) {
    if (axios.isAxiosError(error)) {
    console.log('handleAxiosError(error)');
    if(error.response?.status===401){
      alert('Пользователь не авторизован!');      
    }
    if(error.response?.status===403){
      alert('Пользователь авторизован, но у него нет прав на эту страницу!');
    }
    console.log(error.response?.status);
    //handleAxiosError(error);
    } else {
    console.log('handleUnexpectedError(error)');
    }
  }

  return schedule;
}

export async function GetVisitsByDoctorId(doctorId: string)  {  
  //let props: ScheduleProps = JSON.parse(data);   
  
  try {
    const { data } = await axios.get<VisitForDoctor[]>('https://localhost:7126/api/v1/Visit/bydoctor/'+doctorId,
                                                    {
                                                      headers: {
                                                          'Authorization': "Bearer " + localStorage.getItem('id_token')                                                          
                                                      }
                                                    });
    // const { data } = await axios.get<ScheduleProps>('https://localhost:7126/api/v1/Schedule/simple?begin=2024-01-08&end=2024-01-15');
  
    return data;
  } catch (error) {
    if (axios.isAxiosError(error)) {
    console.log('handleAxiosError(error)');
    if(error.response?.status===401){
      alert('Пользователь не авторизован!');      
    }
    if(error.response?.status===403){
      alert('Пользователь авторизован, но у него нет прав на эту страницу!');
    }
    console.log(error.response?.status);
    //handleAxiosError(error);
    } else {
    console.log('handleUnexpectedError(error)');
    }
  }  
}



