import axios from "axios";
import { PatientShortDto } from "../../Components/Schedule/Props";

function sleep(ms: number) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

export async function GetPatients() : Promise<PatientShortDto[] | undefined>
{  
  try {
    await sleep(300);
    const { data } = await axios.get<PatientShortDto[]>('https://localhost:7126/api/v1/Patient/short');
    return data;
  } catch (error) {
    if (axios.isAxiosError(error)) {
    console.log('handleAxiosError(error)');
    //handleAxiosError(error);
    } else {
    console.log('handleUnexpectedError(error)');
    }
  }
  
}