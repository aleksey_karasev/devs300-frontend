import axios from "axios";
import { FullVisit } from "../../Components/Schedule/VisitProps";

function sleep(ms: number) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

export async function GetVisit(id?:string)
{  
  try {  
    
    const { data } = await axios.get<FullVisit>('https://localhost:7126/api/v1/Visit/'+id);
    return data;
  } catch (error) {
    if (axios.isAxiosError(error)) {
    console.log('handleAxiosError(error)');  
    } else {
    console.log('handleUnexpectedError(error)');
    }
  }  
}

export async function GetFullVisit(id?:string)
{
  try {  
    
    const { data } = await axios.get<FullVisit>('https://localhost:7126/api/v1/Visit/'+id);
    return data;
  } catch (error) {
    if (axios.isAxiosError(error)) {
    console.log('handleAxiosError(error)');  
    } else {
    console.log('handleUnexpectedError(error)');
    }
  }  
}