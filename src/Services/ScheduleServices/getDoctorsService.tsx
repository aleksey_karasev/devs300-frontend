import axios from "axios";
import { DoctorShortDto, PeriodDto } from "../../Components/Schedule/Props";

function sleep(ms: number) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

interface doctorResProps {
  success: boolean;
  data: DoctorShortDto[];
  message: string;
}

export async function GetDoctors() : Promise<DoctorShortDto[] | undefined>
{  
  try {    
    const data = await  axios.get<doctorResProps>('https://localhost:7126/api/v1/Doctor/short');
    console.log("doctors1: "+JSON.stringify(data));
    console.log(data.data.data[0].fio);
    return data.data.data;
    //const response = await fetch("https://localhost:7126/api/v1/Doctor/short");
    //alert(response);
    
    //const dt = await response.json();
    //console.log('doctors: '+JSON.stringify(dt.data));
    //return dt.data;    
    
    //const { data } = await  axios.get<DoctorShortDto[]>('https://localhost:7126/api/v1/Doctor/short');
    //return data;
  } catch (error) {
    if (axios.isAxiosError(error)) {
    console.log('handleAxiosError(error)');  
    } else {
    console.log('handleUnexpectedError(error)');
    }
  } 

}

export async function GetAvailableTimes(dateVisit: string) : Promise<PeriodDto[] | undefined>
{
  try {    
    const { data } = await axios.get<PeriodDto[]>('https://localhost:7126/api/v1/Visit/availtimes/'+dateVisit);
    return data;
  } catch (error) {
    if (axios.isAxiosError(error)) {
    console.log('handleAxiosError(error)');  
    } else {
    console.log('handleUnexpectedError(error)');
    }
  } 
}