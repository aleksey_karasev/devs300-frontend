
import './App.css';
import Schedule from './Components/Schedule/Schedule';
import { BrowserRouter, Link, Route, Routes } from 'react-router-dom';
import EditVisit from './Components/Schedule/EditVisit';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';

import OnlineAppointmentTable from './Components/OnlineAppointment/OnlineAppointmentTable';

import { DoctorsList } from './Components/Doctor/DoctorsList'
import Home from './Components/Home/Home';
import Navigation from './Components/Navigation/Navigation';
import LoginForm from './Components/Login/LoginForm';
import { PatientsList } from './Components/Patient/PatientsList'
import { ReceptionistsList } from './Components/Receptionist/ReceptionistsList'
import ProceduresTable from './Components/VisitProcedure/VisitProcedureTable';
import PatientCardV2 from './Components/PatientCard/PatientCardV2';
import EditVisitForDoctor from './Components/VisitsForDoctor/EditVisitForDoctor';
import PriceList from './Components/Price/PriceList';
import VisitsForDoctor from './Components/VisitsForDoctor/VisitsForDoctor';

function App() {
  

  const queryClient = new QueryClient()   
  
  return (   
    <div className='App'>
      
      <QueryClientProvider client={queryClient}>
        <BrowserRouter>   
        <Navigation/>
          <Routes>
            <Route path='/' element={<Home />} />    
            <Route path='/login' element={<LoginForm />} />    
            <Route path="schedule" element={<Schedule />} /> 
            <Route path="onlineAppointment" element={<OnlineAppointmentTable />} />      
            <Route path="doctors" element={<DoctorsList />} />    
            <Route path="visits/*" element={<EditVisit />} />
            <Route path="patients" element={<PatientsList />} />   
            <Route path="receptionists" element={<ReceptionistsList />} />   
            <Route path="doctorvisit/*" element={<EditVisitForDoctor />} />
            <Route path="patients/patientCard/procedures" element={<ProceduresTable />} />
            <Route path="/patients/patientCard/" element={<PatientCardV2/>} />    
            <Route path="/pricelist" element={<PriceList/>} />
            <Route path="/visitstoday" element={<VisitsForDoctor/>}/>
          </Routes>
        </BrowserRouter>
      </QueryClientProvider>
    </div>
  );
}

export default App;
