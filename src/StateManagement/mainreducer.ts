export interface MainReducerProps {   
    username: string;
    isAuthenticated: boolean; 
    role: string;
    personId?: string;
    personFullname?: string;
    isNewVisit: boolean;
    visitId?: string;
    dateTimeVisit?: string;
    beginSchedule?: string;
    endSchedule?: string;
}

const initialState: MainReducerProps = {  
    username: '',
    isAuthenticated: false,
    role: '',
    personId: undefined,
    personFullname: undefined,
    isNewVisit:false,
    visitId:undefined,
    dateTimeVisit:undefined,
    beginSchedule:undefined,
    endSchedule:undefined
}

const somereducer = (state = initialState, action: any) => {
    console.log('dispatcher received');
    switch (action.type) {      
        case '[BTN_EV] CHANGE_USER':         
            console.log("is BTN_EV CHANGE_USER, payload: "+action.payload);   
            return { ...state, username: action.payload };         
        case '[BTN_EV] CHANGE_AUTH':         
            console.log("is BTN_EV CHANGE_AUTH, payload: "+action.payload);   
            return { ...state, isAuthenticated: action.payload }; 
        case '[BTN_EV] CHANGE_ROLE':         
            console.log("is BTN_EV CHANGE_ROLE, payload: "+action.payload);   
            return { ...state, role: action.payload };                                 
        case '[BTN_EV] CHANGE_PERSONID':         
            console.log("is BTN_EV CHANGE_PERSON, payload: "+action.payload);   
            return { ...state, personId: action.payload };                                 
        case '[BTN_EV] CHANGE_PERSONFIO':         
            console.log("is BTN_EV CHANGE_PERSON, payload: "+action.payload);   
            return { ...state, personFullname: action.payload };     
        case '[BTN_EV] CHANGE_DATETIMEVISIT':         
            console.log("is BTN_EV CHANGE_DATETIMEVISIT, payload: "+action.payload);   
            return { ...state, dateTimeVisit: action.payload }; 
        case '[BTN_EV] CHANGE_ISNEWVISIT':         
            console.log("is BTN_EV CHANGE_ISNEWVISIT, payload: "+action.payload);   
            return { ...state, isNewVisit: action.payload };
        case '[BTN_EV] CHANGE_VISITID':         
            console.log("is BTN_EV CHANGE_VISITID, payload: "+action.payload);   
            return { ...state, visitId: action.payload }; 
        case '[BTN_EV] CHANGE_BEGIN_SCHEDULE':         
            console.log("is BTN_EV CHANGE_BEGIN_SCHEDULE, payload: "+action.payload);   
            return { ...state, beginSchedule: action.payload };  
        case '[BTN_EV] CHANGE_END_SCHEDULE':         
            console.log("is BTN_EV CHANGE_END_SCHEDULE, payload: "+action.payload);   
            return { ...state, endSchedule: action.payload };                 

        default:
            return state;
    }
};
export default somereducer;