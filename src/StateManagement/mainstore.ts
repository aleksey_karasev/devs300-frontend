import { configureStore } from '@reduxjs/toolkit';
import mainreduc from './mainreducer';

const mainstore = configureStore({
    reducer: {        
        namesStore: mainreduc
    },

});
export default mainstore;