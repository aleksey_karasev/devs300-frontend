import { AppBar, Button, Container, IconButton, Menu, MenuItem, Toolbar, Typography } from "@mui/material"
import { Link, useNavigate } from 'react-router-dom';
import { connect, useDispatch } from "react-redux";
import { MainReducerProps } from "../../StateManagement/mainreducer";
import withAuthentication from "../WithAuthentication/WithAuthentication";
import MenuIcon from '@mui/icons-material/Menu';
import { useState } from "react";

interface NavigationProps {
  username: string;
  isAuthenticated: boolean; 
  role: string;
  personId?: string;
  personFullname?: string;
}

const Navigation = (props: NavigationProps) => {
    const navigate = useNavigate();    
    const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
    const open = Boolean(anchorEl);
    const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
      setAnchorEl(event.currentTarget);
    };
    const handleClose = () => {
      setAnchorEl(null);
    };
    const dispatch = useDispatch();

    const logout = () => {
      localStorage.setItem('id_token', '');         
      dispatch({ type: '[BTN_EV] CHANGE_USER', payload:null });
      dispatch({ type: '[BTN_EV] CHANGE_AUTH', payload:false });
      navigate('/login');      
    }

    return <AppBar position="static">
            <Toolbar>
            {props.isAuthenticated && <div>
              <IconButton
                size="large"
                edge="start"
                color="inherit"
                aria-label="menu"
                sx={{ mr: 2 }}
                onClick={handleClick}
              >
                <MenuIcon />
              </IconButton>
              <Menu
                id="basic-menu"
                anchorEl={anchorEl}
                open={open}
                onClose={handleClose}
                MenuListProps={{
                  'aria-labelledby': 'basic-button',
                }}
              >
                <MenuItem onClick={(e)=> navigate('/onlineAppointment')}>Online-запись</MenuItem>
                <MenuItem onClick={(e)=> navigate('/doctors')}>Врачи</MenuItem>
                <MenuItem onClick={(e)=> navigate('/patients')}>Пациенты</MenuItem>
                <MenuItem onClick={(e)=> navigate('/receptionists')}>Регистраторы</MenuItem>
                <MenuItem onClick={(e)=> navigate('/schedule')}>Расписание</MenuItem>
                <MenuItem onClick={(e)=> navigate('/pricelist')}>Прайс-лист</MenuItem>
                <MenuItem onClick={(e)=> navigate('/visitstoday')}>Визиты на сегодня</MenuItem>

              </Menu>
              </div>}              
              <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
                <div onClick={(e)=>navigate('/')}>Стоматология</div>
              </Typography>
              <div> Логин: {props.username}, Роль: {props.role}, ФИО: {props.personFullname}</div>
              {!props.isAuthenticated && <Button color="inherit" onClick={(e)=> navigate('/login')}>Login</Button>}
              {props.isAuthenticated && <Button color="inherit" onClick={(e)=> logout()}>Logout</Button>}
              {/* {props.isAuthenticated && <Button color="inherit" onClick={(e)=> navigate('/login')}>Logout</Button>} */}
            </Toolbar>
          </AppBar>
  
    
    // <Container>
    // {/* <Typography variant="h4" component="h1">
    //   Домашняя страница
    // </Typography> */}
    // <nav>
    //     <ul>
    //     <li>
    //         <Link to={'/schedule'}>Расписание</Link>
    //     </li> 
    //     <li>
    //         <Link to={'/onlineAppointment'}>Online-запись</Link>
    //     </li>
    //     <li>
    //         <Link to={'/doctors'}>Врачи</Link>
    //     </li>
    //     </ul>
    // </nav>   
          
    
    // {/* <Typography variant="body1">
    //   Добро пожаловать на домашнюю страницу!
    // </Typography> */}
   
    // </Container>      

    
};

const mapReduxStateToProps = (globalReduxState: any): MainReducerProps => {
  console.log('globalredux',globalReduxState);
  return {          
      username: globalReduxState.namesStore.username, 
      isAuthenticated: globalReduxState.namesStore.isAuthenticated,
      personId: globalReduxState.namesStore.personId,
      role: globalReduxState.namesStore.role,
      personFullname: globalReduxState.namesStore.personFullname,
      isNewVisit: globalReduxState.namesStore.isNewVisit,
      dateTimeVisit: globalReduxState.namesStore.dateTimeVisit,
      visitId: globalReduxState.namesStore.visitId
  };
}

export default connect(mapReduxStateToProps)(Navigation);