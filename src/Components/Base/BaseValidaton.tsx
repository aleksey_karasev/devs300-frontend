

export const BaseValidaton = ( openSnackbar: (isOpen: boolean) => void, setSnackbarMessage: (message: string) => void ) => {

    const validationSelectedRow = (selectedRow: string | null) =>  {

        if(selectedRow != null && Object.keys(selectedRow).length > 0) {
            return true;
        }
        else {             
            setSnackbarMessage(`Выберите запись`);
            openSnackbar(true);
            return false;
        }
    }

    return {
        validationSelectedRow
    }
}

export default BaseValidaton;