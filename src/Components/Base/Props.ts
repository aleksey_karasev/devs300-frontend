

export interface BaseSnackbarProps {
  isOpen: boolean;
  message: string;
  onClose: () => void;
}

