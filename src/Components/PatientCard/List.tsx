// List.tsx
import React from 'react';
import ListItem from './ListItem';
import "./listWithoutMarkers.css";

interface ListProps {
    items: string[];
}

const List: React.FunctionComponent<ListProps> = ({ items }) => {
    return (
        <ul className="list-without-markers">
            {items.map((item, index) => (
                <ListItem key={index} text={item} />
            ))}
        </ul>
    );
};

export default List;
