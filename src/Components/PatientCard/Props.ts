import { SyntheticEvent } from "react"

export interface VisitProcedureDto {
  id: string
  service: ServiceDto
  countOfThisProcedures: number
  sum: number
  visit: VisitDto
  tooth: Tooth | null
}

export interface Tooth{
  id: string
  name: string
  code: number
}

// export interface Visit {
//     id: string | undefined | null
//     dateTimeBegin: string | undefined | null
//     dateTimeEnd: string | undefined | null
//     dateOfVisit: string | undefined | null
//     periodSimple: string | undefined | null
//     doctor: DoctorDto | undefined | null
//     patient: PatientDto | undefined | null
//     status: number | undefined | null
//     comment: string | undefined | null
//     fromOnline: boolean | undefined | null
//     procedures: Procedure[] | undefined | null
//   }
  
  export interface DoctorDto {
    id: string | undefined | null
    firstName: string | undefined | null
    fatherName: string | undefined | null
    lastName: string | undefined | null
    phoneNumber: string | undefined | null
    createdAt: string | undefined | null
    qualification: string | undefined | null
    email: string  | undefined | null
    passwordHash: string  | undefined | null
  }
  
  export interface PatientDto {
    id: string | undefined | null
    firstName: string | undefined | null
    fatherName: string | undefined | null
    lastName: string | undefined | null
    phoneNumber: string | undefined | null
    createdAt: string | undefined | null
    allergy: string | undefined | null
  }
  
  export interface Procedure {
    id: string | undefined | null
    service: ServiceDto | undefined | null
    countOfThisProcedures: number | undefined | null
    sum: number | undefined | null
  }
  
  export interface ServiceDto {
    id: string | undefined | null
    code: string | undefined | null
    name: string | undefined | null
    price: number | undefined | null
  }

  // export interface ServicePriceCreateRequest {
  //   code: string
  //   name: string
  //   price: number
  // }

  export interface VisitDto {
    id: string  | null
    dateTimeBegin: string  | undefined | null
    dateTimeEnd: string  | undefined | null
    dateOfVisit: string  | undefined | null
    status: number  | undefined | null
    comment: string  | undefined | null
    fromOnline: boolean  | undefined | null
    doctor: DoctorDto  | undefined | null
    patient: PatientDto  | undefined | null
    procedures: VisitProcedureShortDto[] | null
  }

  export interface VisitProcedureShortDto {
    id: string | undefined | null
    service: ServiceDto | undefined | null
    countOfThisProcedures: number | undefined | null
    sum: number | undefined | null
    tooth: Tooth | null
  }

  // export interface DoctorCreateRequest {
  //   id: string  | undefined | null
  //   firstName: string  | undefined | null
  //   fatherName: string  | undefined | null
  //   lastName: string  | undefined | null
  //   phoneNumber: string  | undefined | null
  //   qualification: string  | undefined | null
  //   email: string  | undefined | null
  //   passwordHash: string  | undefined | null
  // }

  // export interface PatientCreateRequest {
  //   id: string  | undefined | null
  //   firstName: string  | undefined | null
  //   fatherName: string  | undefined | null
  //   lastName: string  | undefined | null
  //   email: string  | undefined | null
  //   phoneNumber: string  | undefined | null
  //   allergy: string | undefined | null
  // }

  export interface ProcedureDto {
    service: ServiceDto | undefined | null
    countOfThisProcedures: number | undefined | null
    sum: number | undefined | null
    visit: VisitDto | undefined | null
  }

  export interface VisitProcedureProps {
    isOpen: boolean
    data: CreateOrEditVisitProcedure
    buttonClose: () => void
    buttonAction: () => void
    handleByNameInputChange: (event: React.ChangeEvent<HTMLInputElement>) => void
    commentChange: (event: React.ChangeEvent<HTMLInputElement>) => void
    visitProcedureChange: (event: SyntheticEvent<Element, Event>, value: any) => void
    toothChange: (event: SyntheticEvent<Element, Event>, value: any) => void
    title: string
    text: string
    disabled: boolean;
  }

  export interface CreateOrEditVisitProcedure {
    patient: PatientDto | undefined | null
    doctor: DoctorDto | undefined | null
    tooth: Tooth | undefined | null
    allergy: string | undefined | null
    comment: string | undefined | null
    procedure: VisitProcedureDto | undefined | null
    countOfThisProcedures: number
  }

  export const CreateOrEditVisitProcedureEmpty = {            
    patient: null,
    doctor: null,
    tooth: null,
    allergy: "",
    comment: "",
    procedure: null,
    countOfThisProcedures: 0
}

export interface EditVisitProcedure {
  id: string
  service: ServiceDto
  countOfThisProcedures: number
  sum: number
  visit: VisitDto
  tooth: Tooth
}

// public class VisitProcedureDto
// {

//     /// <summary>
//     /// Зуб
//     /// </summary>
//     public virtual ToothDto? Tooth { get; set; }
// }