// ListItem.tsx
import React from 'react';

interface ListItemProps {
    text: string;
}

const ListItem: React.FunctionComponent<ListItemProps> = ({ text }) => {
    return <li>{text}</li>;
};

export default ListItem;
