import "./gridStyle.css";
import "./noticeTextAreaStyle.css";
import List from './List';

export function PatientCard() {

    const data = ['Item 1', 'Item 2', 'Item 3'];

    return <>
        <div className="grid">
            
            <table>

                <tr>
                    <td> <label>ФИО: </label></td>
                    <td> <input type="text" /></td>
                </tr>
                <tr>
                    <td> <label>Возраст: </label></td>
                    <td> <input type="text" /></td>
                </tr>
                <tr>
                    <td> <label>Пол: </label></td>
                    <td> <input type="text" /></td>
                </tr>
                <tr>
                    <td> <label>Аллергия: </label></td>
                    <td> <input type="text" /></td>
                </tr>

            </table>

            <table>
                <tr>
                    <td> <label>Примечания: </label></td>
                    <td><textarea className="noticeArea" rows={10} placeholder="Введите несколько строк текста..."></textarea></td>
                </tr>
            </table>
           
            <label>История лечения: </label>
            <List items={data} />

            <p/>

        </div>
    </>
}