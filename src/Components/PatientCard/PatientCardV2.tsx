import { Box, Button, Divider, InputLabel, List, ListItem, ListItemText, Paper, Stack, TextField } from '@mui/material';
import { Patient } from '../Patient/Props';
import { useLocation, useNavigate } from "react-router-dom";
import { useEffect, useState } from 'react';
import { VisitDto } from './Props';
import axios from 'axios';
import dayjs from 'dayjs';
import React from 'react';

const getPatient = async (id: string) => {
  const headers = { 'Authorization': 'Bearer ' + localStorage.getItem('id_token') };
  const response = await fetch(`https://localhost:7126/api/v1/Patient/${id}`, { headers });
  return await response.json();
}

const getVisits = async (patientId: string) => {
  const { data } = await axios.get<VisitDto[]>('https://localhost:7126/api/v1/Visit/');
  return data.filter(item => item.patient?.id == patientId);
}

function formatDateTime(value: string | null | undefined) {
  if (!value) {
    return value;
  }
  return dayjs(Date.parse(value)).format("DD.MM.YYYY HH:MM");
}

export default function PatientCardV2() {
  const navigate = useNavigate();
  const location = useLocation();
  const [patient, setPatient] = useState<Patient>();
  const [visits, setVisits] = useState<VisitDto[]>([]);
  const [selectedVisit, setselectedVisit] = useState<VisitDto>();

  const toPatientCard = (visitId: string | null) => {
    navigate('procedures', { state: { id: visitId } });
  }

  useEffect(() => {
    const fetchData = async () => {
      const data = await getPatient(location.state.id);
      setPatient(data);
    };
    fetchData();
  }, []);

  useEffect(() => {
    const fetchData = async () => {
      const data = await getVisits(location.state.id);
      setVisits(data);
    };
    fetchData();
  }, []);

  function handleRowClick(visit: VisitDto): void {
    setselectedVisit(visit);
  }

  const style = {
    py: 0,
    width: '100%',
    borderRadius: 3,
    border: '1px solid',
    borderColor: 'divider',
    backgroundColor: 'background.paper',
  };

  const listItemStyle = { marginTop: '-10', marginBottom: '-10', marginLeft: 15 }

  return (
    <Box display="flex" justifyContent="center">
      <div style={{ width: '80%' }} >
        <Stack component={Paper}>
          <div style={{ marginLeft: '5rem', marginTop: '1rem', display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
            <h1>Карточка пациента</h1>
            <div style={{ marginBottom: '1rem', marginLeft: '1rem', marginTop: '1rem', marginRight: '4rem', textAlign: 'right' }}>              
              <Button variant="contained" style={{ marginRight: '1rem', minWidth: '150px', maxWidth: '350px' }} onClick={() => navigate(-1)}>Назад</Button>
            </div>
          </div>
          <Stack style={{ marginLeft: '20rem', marginRight: '20rem' }}>
            <TextField label="ФИО" value={patient?.lastName + " " + patient?.firstName + " " + patient?.fatherName} margin="normal" />          
            <TextField label="Аллергия" value={patient?.allergy || 'нет'} margin="normal" />
            <InputLabel style={{textAlignLast:'left'}}>История лечения:</InputLabel> 
            <List>
              {visits.map((visit) => (
                <React.Fragment key={visit.id}>
                  <Box sx={style}>
                    <ListItem>
                      <ListItemText primary={"Дата визита: " + formatDateTime(visit.dateOfVisit)} /> 
                      <Button variant="contained" 
                              style={{ minWidth: '150px', maxWidth: '150px' }} 
                              onClick={() => toPatientCard(visit.id)} >Редактировать</Button>
                    </ListItem>
                    <Divider component="li" />
                    <ListItem>
                      <ListItemText primary={"Врач: " + visit.doctor?.lastName + " " + visit.doctor?.firstName?.charAt(0) + ". " + visit.doctor?.fatherName?.charAt(0) + "."} />
                    </ListItem>
                    <ListItem>
                      <ListItemText primary="Выполненые процедуры:" style={{marginTop: -20}}/>
                    </ListItem>
                    {visit?.procedures?.map(procedure => (
                      <React.Fragment key={procedure.id}>
                        <ListItem style={listItemStyle}>
                          <ListItemText primary={"Наименование: " + procedure.service?.name} style={{marginTop:-10}}/>
                        </ListItem>
                        <ListItem style={listItemStyle}>
                          <ListItemText primary={"Зуб: " + (procedure != null && procedure.tooth != null ? procedure.tooth.name : 'не задан')} style={{marginTop: -20}}/>
                        </ListItem>
                        <ListItem style={listItemStyle}>
                          <ListItemText primary={"Колличество: " + procedure.countOfThisProcedures} style={{marginTop: -20}}/>
                        </ListItem>
                      </React.Fragment>
                    ))}
                  </Box>
                </React.Fragment>
              ))}
            </List>
          </Stack>
        </Stack>
        <div>
        </div>
      </div>
    </Box>
  );
}
