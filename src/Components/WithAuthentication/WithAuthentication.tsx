import React, { ComponentType } from 'react';
import LoginForm from '../Login/LoginForm';



interface AuthProps {
  isAuthenticated: boolean;
}

const withAuthentication = <P extends AuthProps>(
  WrappedComponent: ComponentType<P>
) => {
  return class HOC extends React.Component<Omit<P, keyof AuthProps> & AuthProps> {
    render() {
      const { isAuthenticated, ...props } = this.props;
      console.log('HOC');
      if(isAuthenticated)
        console.log('isAuthenticated');
      else
        console.log('is not authenticated');
      // Если пользователь аутентифицирован, рендерим обернутый компонент, иначе возвращаем null или что-то другое
      return isAuthenticated ? <WrappedComponent {...props as P} /> : <LoginForm/>;
    }
  };
};

export default withAuthentication;