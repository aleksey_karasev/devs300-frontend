import { Container, Typography } from "@mui/material";
import Navigation from "../Navigation/Navigation";
import { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import { MainReducerProps } from "../../StateManagement/mainreducer";
import { connect } from "react-redux";
import VisitsForDoctor from "../VisitsForDoctor/VisitsForDoctor";
import Schedule from "../Schedule/Schedule";

interface HomeProps {
    username: string;
    isAuthenticated: boolean; 
    role: string;
  }

const Home = (props: HomeProps) => {
    const navigate = useNavigate();
    
    useEffect(() => {        
        async function checkLogin() {
            if(localStorage.getItem('id_token')===undefined || props.isAuthenticated!=true) {
                navigate('/login');
            } else {
                try {
                    // await axios.get('https://localhost:7071/auth/check',
                    //                                                 {
                    //                                                   headers: {
                    //                                                       'Authorization': "Bearer " + localStorage.getItem('id_token')                                                          
                    //                                                   }
                    //                                                 });       
                    //alert('autorized');                                                                               
                   } catch (error) {
                   
                    if (axios.isAxiosError(error)) {
                        console.log('handleAxiosError(error)');
                        if(error.response?.status===401){
                            //alert('Пользователь не авторизован!');     
                            navigate('/login');
                        }
                        if(error.response?.status===403){
                            //alert('Пользователь авторизован, но у него нет прав на эту страницу!');
                            navigate('/login');
                        }
                        console.log(error.response?.status);
                    
                    } else {
                        console.log('handleUnexpectedError(error)');
                    }
                  }

                //alert('id_token is not undefined! it is '+localStorage.getItem('id_token'));
            }
        }
        checkLogin();
     }, []);

    return  <Container>
    {/* <Navigation/>    */}
    {/* <Typography variant="h4" component="h1">
      Домашняя страница
    </Typography>   */}
    {props.role==='doctor' && <VisitsForDoctor/>}
    {props.role==='receptionist' && <Schedule/> }
    
    {/* <Typography variant="body1">
      Добро пожаловать на домашнюю страницу!
    </Typography> */}
   
    </Container>      
};


const mapReduxStateToProps = (globalReduxState: any): MainReducerProps => {
    console.log('globalredux',globalReduxState);
    return {          
        username: globalReduxState.namesStore.username, 
        isAuthenticated: globalReduxState.namesStore.isAuthenticated,
        personId: globalReduxState.namesStore.personId,
        role: globalReduxState.namesStore.role,
        isNewVisit: globalReduxState.namesStore.isNewVisit,
        dateTimeVisit: globalReduxState.namesStore.dateTimeVisit,
        visitId: globalReduxState.namesStore.visitId
    };
  }
  
export default connect(mapReduxStateToProps)(Home);

