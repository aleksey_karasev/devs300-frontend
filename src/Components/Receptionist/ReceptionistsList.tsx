import { useState, useEffect } from "react";

import { Receptionist } from './Props';
import { ReceptionistModalWindow } from './ReceptionistModalWindow';
import ReceptionistCRUD from './ReceptionistCRUD'; 
import BaseValidaton from '../Base/BaseValidaton'; 
import BaseSnackbar from '../Base/BaseSnackbar'; 

import {TableContainer, Table, TableHead, TableBody, TableRow, TableCell, Paper, Button, Box } from "@mui/material"

  
export const ReceptionistsList = () => 
{
    //Переменные всплывающего окна, хранятся здесь, но при CRUD операциях устанавливаются сообщения
    const [snackbarOpen, setSnackbarOpen] = useState(false);
    const [snackbarMessage, setSnackbarMessage] = useState('');
    const [timeChange, setTimeChange] = useState(new Date());

    //CRUD функционал с доктором
    const 
    { 
        newReceptionist, setNewReceptionist,
        getAllReceptionists,
        CreateReceptionist, 
        EditReceptionist, 
        DeleteReceptionist, 
        receptionistId, setReceptionistId,
        handleInputChange

    } = ReceptionistCRUD(setSnackbarOpen, setSnackbarMessage,setTimeChange);

    const 
    { 
        validationSelectedRow

    } = BaseValidaton(setSnackbarOpen, setSnackbarMessage);
    
    
    const [tableData, setReceptionistsList] = useState<Receptionist[]>([]);

    const [showCreateWin, setCreateWin] = useState<boolean>(false);
    const [showEditWin, setEditWin] = useState<boolean>(false);
    const [showDeleteeWin, setDeleteWin] = useState<boolean>(false);

    const clickCreate = () =>
    {
        setNewReceptionist({});
        setReceptionistId(null);
        setCreateWin(true);  
    }

    const clickEdit = () =>
    {
        if(validationSelectedRow(receptionistId)){ 
            setEditWin(true);    
        }
    }

    const clickDelete = () =>
    {
        if(validationSelectedRow(receptionistId)){ 
            setDeleteWin(true); 
        }
    }

    useEffect(() => {
        const fetchData = async () => {
            const data = await getAllReceptionists();
            setReceptionistsList(data);
        };
        fetchData();
    }, [timeChange]);

    //Нажатие на доктора из списка
    const handleRowClick = (id: string) => {
        setReceptionistId(id === receptionistId ? null : id);

        const selectedReceptionist = tableData.find(receptionist => receptionist.id === id);
        if (selectedReceptionist) {
            setNewReceptionist(selectedReceptionist);
        }
    };

    return (
        <Box display="flex" justifyContent="center">
            <div style={{ width: '80%' }}>
                <TableContainer component={Paper} style={{ width: '100%' }}>

                    <div style={{  marginLeft: '5rem', marginTop: '1rem', display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                        <h1>Врачи</h1>
                        <div style={{ marginBottom: '1rem', marginLeft: '1rem', marginTop: '1rem', marginRight: '4rem', textAlign: 'right' }}>
                            <Button variant="contained" color="primary" style={{ marginRight: '1rem' }} onClick={clickCreate}>Создать</Button>
                            <Button variant="contained" color="info" style={{ marginRight: '1rem' }} onClick={clickEdit}>Редактировать</Button>
                            <Button variant="contained" color="secondary" style={{ marginRight: '1rem' }} onClick={clickDelete}>Удалить</Button>
                        </div>
                    </div>

                    <Table aria-label='simple table'>
                        <TableHead>
                            <TableRow>
                                <TableCell><b>Имя</b></TableCell>
                                <TableCell><b>Фамилия</b></TableCell>
                                <TableCell><b>Отчество</b></TableCell>
                                <TableCell><b>Номер телефона</b></TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {tableData.map(row => (
                                <TableRow
                                    key={row.id}
                                    onClick={() => handleRowClick(row.id)}
                                    sx={{ cursor: 'pointer', backgroundColor: row.id === receptionistId ? '#91D1EC' : 'transparent' }}
                                >
                                    <TableCell>{row.firstName}</TableCell>
                                    <TableCell>{row.lastName}</TableCell>
                                    <TableCell>{row.fatherName}</TableCell>
                                    <TableCell>{row.phoneNumber}</TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
                <ReceptionistModalWindow
                    isOpen={showCreateWin}
                    buttonClose={() => {setCreateWin(false)}}
                    buttonAction={CreateReceptionist}
                    newData={newReceptionist}
                    handleInputChange={handleInputChange}
                    title={"Создание нового врача"}
                    titleButton={"Создать"}
                    disabled={false}
                />
                <ReceptionistModalWindow
                    isOpen={showEditWin}
                    buttonClose={() => {setEditWin(false)}}
                    buttonAction={EditReceptionist}
                    newData={newReceptionist}
                    handleInputChange={handleInputChange}
                    title={"Редактирование врача"}
                    titleButton={"Изменить"}
                    disabled={false}
                />
                <ReceptionistModalWindow
                    isOpen={showDeleteeWin}
                    buttonClose={() => {setDeleteWin(false)}}
                    buttonAction={DeleteReceptionist}
                    newData={newReceptionist}
                    handleInputChange={handleInputChange}
                    title={"Удаление врача. Вы действительно хотите удалить врача?"}
                    titleButton={"Удалить"}
                    disabled={true}
                />
                <BaseSnackbar
                    isOpen={snackbarOpen}
                    message={snackbarMessage}
                    onClose={() => setSnackbarOpen(false)}
                />
            </div>
        </Box>
    );
}

export default ReceptionistsList;