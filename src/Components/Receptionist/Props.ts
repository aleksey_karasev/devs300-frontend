
export interface Receptionist {
  id: string;
  firstName: string;
  fatherName: string;
  lastName: string;
  phoneNumber: string;
  email: string;
}

export interface ReceptionistFormModalProps {
  isOpen: boolean;
  buttonClose: () => void;
  buttonAction: () => void;
  newData: Partial<Receptionist>;
  handleInputChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  title: string;
  titleButton: string;
  disabled: boolean;
}

