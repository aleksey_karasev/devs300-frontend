import { useState } from "react";
import { Receptionist } from './Props';

export const ReceptionistCRUD = ( openSnackbar: (isOpen: boolean) => void, setSnackbarMessage: (message: string) => void, setTimeChange: (time:Date)=>void ) => {

    const [receptionistId, setReceptionistId] = useState<string | null>(null);

    const [newReceptionist, setNewReceptionist] = useState<Partial<Receptionist>>({});


    const CreateReceptionist = async () => {

        await messageHandleReceptionist('Доктор успешно создан', 'Не удалось создать доктора', "https://localhost:7126/api/v1/Receptionist", 'POST');
    };

    const EditReceptionist = async () => {
       
        await messageHandleReceptionist('Доктор успешно изменен', 'Не удалось изменить доктора', "https://localhost:7126/api/v1/Receptionist/", 'PUT', receptionistId);
    };

    const DeleteReceptionist = async () => {
        
        await messageHandleReceptionist('Доктор успешно удален', 'Не удалось удалить доктора', "https://localhost:7126/api/v1/Receptionist/", 'DELETE', receptionistId);
    };

    //После запроса вызывает сообщение о проделанной работе
    const messageHandleReceptionist = async (messSeccess: string, messError: string, url: string, p_mehtod: string, id?:string | null) => {

        var response = await queryReceptionist(url, p_mehtod, receptionistId)

        if (response?.ok) {
            reloadPage();
            setSnackbarMessage(messSeccess); 
        } else {
            setSnackbarMessage(`${messError}: ${response?.statusText}`);
        }
    
        openSnackbar(true);
    }

    //Выполняет запрос
    const queryReceptionist = async (url: string, p_mehtod: string, id?:string | null) => {
        
        try 
        {            
            return await fetch(`${url}${id ?? ''}`, {
                method: p_mehtod,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer '+localStorage.getItem('id_token')
                },
                body: JSON.stringify(newReceptionist)
            });

        } catch (error) {
            console.error("An error occurred:", error); 
        }

    };

    const getAllReceptionists = async () => 
    {
        const headers = { 'Authorization': 'Bearer '+localStorage.getItem('id_token') };
        const response = await fetch("https://localhost:7126/api/v1/Receptionist", { headers });
        return await response.json();
    }


    const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { name, value } = event.target;
        setNewReceptionist(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    function reloadPage() {
        setTimeChange(new Date());
        // setTimeout(() => {
        //     window.location.reload();
        // }, 700); 
    }

    return {
        newReceptionist, setNewReceptionist,
        getAllReceptionists,
        CreateReceptionist, 
        EditReceptionist, 
        DeleteReceptionist, 
        receptionistId, setReceptionistId,
        handleInputChange
    }
}

export default ReceptionistCRUD;