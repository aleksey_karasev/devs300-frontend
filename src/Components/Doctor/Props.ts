
export interface Doctor {
  id: string;
  firstName: string;
  fatherName: string;
  lastName: string;
  phoneNumber: string;
  email: string;
  qualification: string;
  outerId: string;
}

export interface DoctorFormModalProps {
  isOpen: boolean;
  buttonClose: () => void;
  buttonAction: () => void;
  newData: Partial<Doctor>;
  handleInputChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  title: string;
  titleButton: string;
  disabled: boolean;
}

