import { useState } from "react";
import { Doctor } from './Props';

export const DoctorCRUD = ( openSnackbar: (isOpen: boolean) => void, setSnackbarMessage: (message: string) => void, setTimeChange: (time:Date)=>void ) => {

    const [doctorId, setDoctorId] = useState<string | null>(null);
    const [doctorOuterId, setDoctorOuterId] = useState<string | null>(null);

    //Данные нового врача
    const [newDoctor, setNewDoctor] = useState<Partial<Doctor>>({});


    //Функционал создания доктора
    const CreateDoctor = async () => {
        await messageHandleDoctor('Доктор успешно создан', 'Не удалось создать доктора', "https://localhost:7071/api/doctors/", 'POST');
        //await messageHandleDoctor('Доктор успешно создан', 'Не удалось создать доктора', "https://localhost:7126/api/v1/Doctor", 'POST');
    };

    //Функционал изменения доктора
    const EditDoctor = async () => {
       
        await messageHandleDoctor('Доктор успешно изменен', 'Не удалось изменить доктора', "https://localhost:7071/api/doctors/", 'PUT', doctorOuterId);
        //await messageHandleDoctor('Доктор успешно изменен', 'Не удалось изменить доктора', "https://localhost:7126/api/v1/Doctor/", 'PUT', doctorId);
    };

    //Функционал удаления доктора
    const DeleteDoctor = async () => {
        
        await messageHandleDoctor('Доктор успешно удален', 'Не удалось удалить доктора', "https://localhost:7071/api/doctors/", 'DELETE', doctorOuterId);
        //await messageHandleDoctor('Доктор успешно удален', 'Не удалось удалить доктора', "https://localhost:7126/api/v1/Doctor/", 'DELETE', doctorId);
    };

    //После запроса вызывает сообщение о проделанной работе
    const messageHandleDoctor = async (messSeccess: string, messError: string, url: string, p_mehtod: string, id?:string | null) => {

        var response = await queryDoctor(url, p_mehtod, doctorOuterId??doctorId)
        
        if (response?.ok) {
            reloadPage();
            setSnackbarMessage(messSeccess); 
        } else {
            setSnackbarMessage(`${messError}: ${response?.statusText}`);
        }
        
        openSnackbar(true);
    }

    function reloadPage() {
        console.log('setTimeChange '+new Date());
        setTimeChange(new Date());
        // setTimeout(() => {
        //     window.location.reload();
        // }, 700); 
   }

    //Выполняет запрос
    const queryDoctor = async (url: string, p_mehtod: string, id?:string | null) => {
        
        console.log(p_mehtod)
        try 
        {            
            return await fetch(`${url}${id ?? ''}`, {
                method: p_mehtod,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer '+localStorage.getItem('id_token')
                },
                body: JSON.stringify(newDoctor)
            });

        } catch (error) {
            console.error("An error occurred:", error); 
        }

    };

    const getAllDoctors = async () => 
    {
        const headers = { 'Authorization': 'Bearer '+localStorage.getItem('id_token') };
        const response = await fetch("https://localhost:7126/api/v1/Doctor", { headers });
        //console.log('doctors response: '+response.json());
        //const response = await fetch("https://localhost:6001/api/v1/Doctor", { headers });
        return await response.json();
    }


    const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { name, value } = event.target;
        setNewDoctor(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    

    return {
        newDoctor, setNewDoctor,
        getAllDoctors,
        CreateDoctor, 
        EditDoctor, 
        DeleteDoctor, 
        doctorId, setDoctorId,
        doctorOuterId, setDoctorOuterId,
        handleInputChange
    }
}

export default DoctorCRUD;
