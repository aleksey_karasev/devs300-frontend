import { useState, useEffect } from "react";

import { Doctor } from './Props';
import { DoctorModalWindow } from './DoctorModalWindow';
import DoctorCRUD from './DoctorCRUD'; 
import BaseValidaton from '../Base/BaseValidaton'; 
import BaseSnackbar from '../Base/BaseSnackbar'; 

import {TableContainer, Table, TableHead, TableBody, TableRow, TableCell, Paper, Button, Box } from "@mui/material"

  
export const DoctorsList = () => 
{
    //Переменные всплывающего окна, хранятся здесь, но при CRUD операциях устанавливаются сообщения
    const [snackbarOpen, setSnackbarOpen] = useState(false);
    const [snackbarMessage, setSnackbarMessage] = useState('');
    const [timeChange, setTimeChange] = useState(new Date());

    //CRUD функционал с доктором
    const 
    { 
        newDoctor, setNewDoctor, 
        getAllDoctors, 
        CreateDoctor, 
        EditDoctor, 
        DeleteDoctor,
        doctorId, setDoctorId, 
        doctorOuterId, setDoctorOuterId,
        handleInputChange 

    } = DoctorCRUD(setSnackbarOpen, setSnackbarMessage, setTimeChange);

    //Валидация доктора
    const 
    { 
        validationSelectedRow

    } = BaseValidaton(setSnackbarOpen, setSnackbarMessage);
    
    
    const [tableData, setDoctorsList] = useState<Doctor[]>([]);

    const [showCreateWin, setCreateWin] = useState<boolean>(false);
    const [showEditWin, setEditWin] = useState<boolean>(false);
    const [showDeleteeWin, setDeleteWin] = useState<boolean>(false);

    const clickCreate = () =>
    {
        setNewDoctor({});
        setDoctorId(null);
        setCreateWin(true);  
    }

    const clickEdit = () =>
    {
        if(validationSelectedRow(doctorId)){ 
            setEditWin(true);    
        }
    }

    const clickDelete = () =>
    {
        if(validationSelectedRow(doctorId)){ 
            setDeleteWin(true); 
        }
    }

    //Подгружаю список докторов
    useEffect(() => {
        const fetchData = async () => {
            const data = await getAllDoctors();
            console.log('doctorus:');
            console.log(data);
            setDoctorsList(data.data);
        };
        fetchData();
    }, [timeChange]);

    //Нажатие на доктора из списка
    const handleRowClick = (id: string) => {
        setDoctorId(id === doctorId ? null : id);

        const selectedDoctor = tableData.find(doctor => doctor.id === id);
        if (selectedDoctor) {
            console.log("outerId="+selectedDoctor.outerId);
            setDoctorOuterId(selectedDoctor.outerId);
            setNewDoctor(selectedDoctor);
        }
    };

    return (
        <Box display="flex" justifyContent="center">
            <div style={{ width: '80%' }}>
                <TableContainer component={Paper} style={{ width: '100%' }}>

                    <div style={{  marginLeft: '5rem', marginTop: '1rem', display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                        <h1>Врачи</h1>
                        <div style={{ marginBottom: '1rem', marginLeft: '1rem', marginTop: '1rem', marginRight: '4rem', textAlign: 'right' }}>
                            <Button variant="contained" color="primary" style={{ marginRight: '1rem' }} onClick={clickCreate}>Создать</Button>
                            <Button variant="contained" color="info" style={{ marginRight: '1rem' }} onClick={clickEdit}>Редактировать</Button>
                            <Button variant="contained" color="secondary" style={{ marginRight: '1rem' }} onClick={clickDelete}>Удалить</Button>
                        </div>
                    </div>

                    <Table aria-label='simple table'>
                        <TableHead>
                            <TableRow>
                                <TableCell><b>Имя</b></TableCell>
                                <TableCell><b>Фамилия</b></TableCell>
                                <TableCell><b>Отчество</b></TableCell>
                                <TableCell><b>Номер телефона</b></TableCell>
                                <TableCell><b>Квалификация</b></TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {tableData.map(row => (
                                <TableRow
                                    key={row.id}
                                    onClick={() => handleRowClick(row.id)}
                                    sx={{ cursor: 'pointer', backgroundColor: row.id === doctorId ? '#91D1EC' : 'transparent' }}
                                >
                                    <TableCell>{row.firstName}</TableCell>
                                    <TableCell>{row.lastName}</TableCell>
                                    <TableCell>{row.fatherName}</TableCell>
                                    <TableCell>{row.phoneNumber}</TableCell>
                                    <TableCell>{row.qualification}</TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
                <DoctorModalWindow
                    isOpen={showCreateWin}
                    buttonClose={() => {setCreateWin(false)}}
                    buttonAction={CreateDoctor}
                    newData={newDoctor}
                    handleInputChange={handleInputChange}
                    title={"Создание нового врача"}
                    titleButton={"Создать"}
                    disabled={false}
                />
                <DoctorModalWindow
                    isOpen={showEditWin}
                    buttonClose={() => {setEditWin(false)}}
                    buttonAction={EditDoctor}
                    newData={newDoctor}
                    handleInputChange={handleInputChange}
                    title={"Редактирование врача"}
                    titleButton={"Изменить"}
                    disabled={false}
                />
                <DoctorModalWindow
                    isOpen={showDeleteeWin}
                    buttonClose={() => {setDeleteWin(false)}}
                    buttonAction={DeleteDoctor}
                    newData={newDoctor}
                    handleInputChange={handleInputChange}
                    title={"Удаление врача. Вы действительно хотите удалить врача?"}
                    titleButton={"Удалить"}
                    disabled={true}
                />
                <BaseSnackbar
                    isOpen={snackbarOpen}
                    message={snackbarMessage}
                    onClose={() => setSnackbarOpen(false)}
                />
            </div>
        </Box>
    );
}

export default DoctorsList;