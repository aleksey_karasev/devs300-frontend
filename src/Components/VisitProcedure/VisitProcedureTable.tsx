import { useState, useEffect } from "react";
import { Box, Button } from '@mui/material';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { ThemeProvider } from '@mui/material/styles';
import { createTheme } from '@mui/material/styles';
import { grey } from '@mui/material/colors';
import { VisitDto, VisitProcedureShortDto } from './Props';
import VisitProcedureCRUD from "./VisitProcedureCRUD";
import VisitProcedureWindow from "./VisitProcedureWindow";
import BaseSnackbar from "../Base/BaseSnackbar";
import { useLocation, useNavigate } from "react-router-dom";
import axios from "axios";

const theme = createTheme({
    components: {
        MuiTableCell: {
            styleOverrides: {
                root: {
                    borderRight: `1px solid`,
                    borderTop: `1px solid`,
                    borderColor: grey[300]
                },
            },
        },

        MuiTable: {
            styleOverrides: {
                root: {
                    spacing: 10
                },
            },
        }
    },
});

const getVisit = async (visitId: string) => {
    const { data } = await axios.get<VisitDto>('https://localhost:7126/api/v1/Visit/' + visitId);
    return data;
}

export default function ProceduresTable() {

    const navigate = useNavigate();
    const location = useLocation();

    const [snackbarOpen, setSnackbarOpen] = useState(false);
    const [snackbarMessage, setSnackbarMessage] = useState('');
    const [showCreateWindow, setShowCreateWindow] = useState(false);
    const [showEditWindow, setShowEditWindow] = useState(false);    
    const [timeChange, setTimeChange] = useState(new Date());

    useEffect(() => {
        const fetchData = async () => {
            const data = await getVisit(location.state.id);
            setVisit(data);

            setSelectedData({
                patient: data?.patient,
                doctor: data?.doctor,
                service: null,
                allergy: data?.patient?.allergy,
                comment: null,
                tooth: null,
                countOfThisProcedures: 1});
        };
        fetchData();
    }, [timeChange]);

    const {
            selectedVisitProcedure, setSelectedVisitProcedure,
            selectedData, setSelectedData,
            visit, setVisit,
            visitProcedures, setVisitProcedures,
            Create,
            Edit,
            Delete,
            handleByNameInputChange,
            visitProcedureChange,
            toothChange
        } = VisitProcedureCRUD(setSnackbarOpen, setSnackbarMessage, setTimeChange);

    const handleRowClick = (visitProcedure: VisitProcedureShortDto) => {
        if (visitProcedure.id == selectedVisitProcedure?.id) {
            setSelectedVisitProcedure(null);
            setSelectedData({
                patient: visit?.patient,
                doctor: visit?.doctor,
                service: null,
                allergy: visit?.patient?.allergy,
                comment: visit?.comment,
                tooth: null,
                countOfThisProcedures: 1});
        }
        else {
            const visitProcedureDto = visitProcedures.find(item => item.id == visitProcedure.id);
            if(!visitProcedureDto){
                return;
            }

            setSelectedVisitProcedure(visitProcedureDto);
            setSelectedData({
                patient: visit?.patient,
                doctor: visit?.doctor,
                service: visitProcedureDto.service,
                allergy: visit?.patient?.allergy,
                comment: visit?.comment,
                tooth: visitProcedure.tooth,
                countOfThisProcedures: visitProcedure ? visitProcedure.countOfThisProcedures : 1});
        }            
    };
    return (
        <Box display="flex" justifyContent="center" >
            <div style={{ width: '100%' }}>
                <ThemeProvider theme={theme}>
                    <TableContainer component={Paper}>
                        <div style={{ marginLeft: '5rem', marginTop: '1rem', display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                            <h1>Редактирование процедур</h1>
                            <div style={{ marginBottom: '1rem', marginLeft: '1rem', marginTop: '1rem', marginRight: '4rem', textAlign: 'right' }}>
                                <Button variant="contained"
                                    style={{ marginRight: '1rem', minWidth: '150px', maxWidth: '150px' }}
                                    color="primary"
                                    onClick={() => setShowCreateWindow(true)}>Добавить</Button>
                                <Button variant="contained"
                                    style={{ marginRight: '1rem', minWidth: '150px', maxWidth: '150px' }}
                                    onClick={() => setShowEditWindow(true)}
                                    color="secondary"
                                    disabled={!selectedVisitProcedure}>Редактировать</Button>                                
                                <Button variant="contained"
                                    style={{ marginRight: '1rem', minWidth: '150px', maxWidth: '150px' }}
                                    onClick={Delete}
                                    color="error"
                                    disabled={!selectedVisitProcedure}>Удалить</Button>
                                <Button 
                                    variant="contained" 
                                    style={{ marginRight: '1rem', minWidth: '150px', maxWidth: '150px' }} 
                                    color="info"
                                    // onClick={() => navigate(-1)}>Пациент</Button>
                                    onClick={()=> navigate('/patients/patientCard/', {state:{id:visit?.patient?.id}})}>Пациент</Button>
                            </div>
                        </div>
                        <Table>
                            <TableHead>
                                <TableRow sx={{ cursor: 'pointer', backgroundColor: 'transparent' }}>
                                    <TableCell align="center"><b>Зуб</b></TableCell>
                                    <TableCell align="center"><b>Процедура</b></TableCell>
                                    <TableCell align="center"><b>Колличество</b></TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {visit?.procedures?.map((procedure) => (
                                    <TableRow key={procedure.id}
                                        onClick={() => handleRowClick(procedure)}
                                        sx={{ cursor: 'pointer', backgroundColor: procedure.id === selectedVisitProcedure?.id ? '#91D1EC' : 'transparent' }}>
                                        <TableCell align="right">{procedure.tooth?.name}</TableCell>
                                        <TableCell align="right">{procedure.service?.name}</TableCell>
                                        <TableCell align="right">{procedure.countOfThisProcedures}</TableCell>
                                    </TableRow>))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                </ThemeProvider>
                <VisitProcedureWindow
                    isOpen={showCreateWindow}
                    buttonClose={() => { setShowCreateWindow(false) }}
                    buttonAction={Create}
                    data={selectedData}
                    handleByNameInputChange={handleByNameInputChange}
                    visitProcedureChange={visitProcedureChange}
                    toothChange={toothChange}
                    title={"Добавление процедуры"}
                    text={"Добавить"}
                    disabled={false} />
                <VisitProcedureWindow
                    isOpen={showEditWindow}
                    buttonClose={() => { setShowEditWindow(false) }}
                    buttonAction={Edit}
                    data={selectedData}
                    handleByNameInputChange={handleByNameInputChange}
                    visitProcedureChange={visitProcedureChange}
                    toothChange={toothChange}
                    title={"Редактирование процедуры"}
                    text={"Изменить"}
                    disabled={false} />
                <BaseSnackbar
                    isOpen={snackbarOpen}
                    message={snackbarMessage}
                    onClose={() => setSnackbarOpen(false)} />
            </div>
        </Box>
    );
}