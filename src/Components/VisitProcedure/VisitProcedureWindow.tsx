import { useEffect, useState } from 'react';
import { ServicePriceDto, Tooth, VisitProcedureProps } from './Props';
import { Button, Box, Modal, TextField, Autocomplete, Stack } from "@mui/material"

export const DoctorModalWindow: React.FC<VisitProcedureProps> = ({ isOpen, data, buttonClose, buttonAction, handleByNameInputChange, visitProcedureChange, toothChange, title, text, disabled }) => {

    const [services, setServices] = useState<ServicePriceDto[]>([]);
    const [teeth, setTeeth] = useState<Tooth[]>([]);

    useEffect(() => {
        const fetchRows = async () => {
            const response = await fetch("https://localhost:7126/api/v1/ServicePrice");
            const data = await response.json();
            setServices(data);
        };
        fetchRows();
    }, []);

    useEffect(() => {
        const fetchRows = async () => {
            const response = await fetch("https://localhost:7126/api/v1/Teeth");
            const teeth = await response.json();
            setTeeth(teeth);
        };
        fetchRows();
    }, []);

    const isTooth = (option: Tooth, value: Tooth) => option.code === value.code;
    const isServicePrice = (option: ServicePriceDto, value: ServicePriceDto) => option.id == value.id;

    return <Modal
        open={isOpen}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description" >
        <Box
            sx={{
                position: 'absolute',
                top: '50%',
                left: '50%',
                transform: 'translate(-50%, -50%)',
                width: 400,
                bgcolor: 'background.paper',
                boxShadow: 24,
                p: 4,
            }}>
            <h2 id="modal-modal-title">{title}</h2>
            <TextField
                label="Врач"
                value={data?.doctor?.lastName + " " + data?.doctor?.firstName + " " + data?.doctor?.fatherName}
                fullWidth
                margin="normal"/>
            <TextField
                label="ФИО"
                value={data?.patient?.lastName + " " + data?.patient?.firstName + " " + data?.patient?.fatherName}
                fullWidth
                margin="normal"/>
            <TextField
                label="Аллергия"
                name="allergy"
                value={data?.allergy || ''}
                onChange={handleByNameInputChange}
                fullWidth
                margin="normal"
                disabled={disabled} />
            <Stack spacing={3} sx={{ marginTop: 2, marginBottom: 1 }}>
                <Autocomplete
                    id="tooth"
                    fullWidth
                    options={teeth}
                    isOptionEqualToValue={isTooth}
                    value={data?.tooth}
                    getOptionLabel={(item) => item.name + ` (${item.code})`}
                    onChange={(e, newValue) => { toothChange(e, newValue); }}
                    renderInput={(params) => <TextField {...params} label="Зуб" required />}
                    disabled={disabled} />
                <Autocomplete
                    id="services"
                    fullWidth
                    options={services}
                    isOptionEqualToValue={isServicePrice}
                    value={data.service}
                    getOptionLabel={(service) => service.name}
                    onChange={(e, newValue) => { visitProcedureChange(e, newValue); }}
                    renderInput={(params) => <TextField {...params} label="Процедура" required />}
                    disabled={disabled} />
            </Stack>

            <TextField
                label="Колличество"
                name="countOfThisProcedures"
                type="number"
                value={data.countOfThisProcedures}
                onChange={handleByNameInputChange}
                fullWidth
                margin="normal"
                disabled={disabled} />
            <TextField
                label="Примечания"
                name="comment"
                value={data?.comment}
                onChange={handleByNameInputChange}
                fullWidth
                margin="normal"
                multiline
                rows={5}
                disabled={disabled} />
            <Box mt={2} display="flex" justifyContent="flex-end">
                <Button variant="contained" color="primary" onClick={buttonAction}>{text}</Button>
                <Button variant="contained" color="primary" onClick={buttonClose} style={{ marginLeft: '10px' }}>Закрыть</Button>
            </Box>
        </Box>
    </Modal>
};

export default DoctorModalWindow;