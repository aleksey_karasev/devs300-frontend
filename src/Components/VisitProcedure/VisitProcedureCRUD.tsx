import { SyntheticEvent, useEffect, useState } from "react";
import { CreateOrEditVisitProcedure, CreateOrEditVisitProcedureEmpty, VisitProcedureDto, VisitDto, VisitProcedureRequest, ProcedureFromVisitEditRequest } from "./Props";
import axios from "axios";

const getVisitProcedures = async () => {
    const { data } = await axios.get<VisitProcedureDto[]>('https://localhost:7126/api/v1/VisitProcedures');
    return data;
  }

export const VisitProcedureCRUD = (openSnackbar: (isOpen: boolean) => void, setSnackbarMessage: (message: string) => void,setTimeChange: (time:Date)=>void ) => {

    const [selectedVisitProcedure, setSelectedVisitProcedure] = useState<VisitProcedureDto | null>(null);
    const [visit, setVisit] = useState<VisitDto>();
    const [visitProcedures, setVisitProcedures] = useState<VisitProcedureDto[]>([]);
    const [selectedData, setSelectedData] = useState<CreateOrEditVisitProcedure>(CreateOrEditVisitProcedureEmpty);   

    useEffect(() => {
        const fetchData = async () => {
          const data = await getVisitProcedures();
          setVisitProcedures(data);
        };
        fetchData();
      }, []);
    

    const Delete = async () => {
        const url = `https://localhost:7126/api/v1/VisitProcedures/${selectedVisitProcedure?.id}`;
        fetch(url, {
            method: 'DELETE',
            headers: { 'Content-Type': 'application/json' },
        })
            .then(async response => {
                if (response.ok) {
                    reloadPage();
                    setSnackbarMessage('Online запись успешно удалена');
                }
                else {
                    return response.text().then(text => { throw new Error(text) })
                }
            }).catch(error => {
                setSnackbarMessage('Не удалось удалить online запись ' + error);
            });

        openSnackbar(true);
    };

    const Create = async () => {
        if(!selectedData.service){
            return;
        }
        if(!selectedData.tooth){
            return;
        }

        const request : VisitProcedureRequest = {
            service: selectedData.service,
            countOfThisProcedures: selectedData.countOfThisProcedures,
            sum: selectedData.service?.price * selectedData.countOfThisProcedures,
            tooth: selectedData.tooth
        }; 

        const options = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(request)
        };

        fetch('https://localhost:7126/api/v1/Visit/AddProcedureToVisit/' + visit?.id, options)
        .then(async response => {
            if (response.ok) {
                reloadPage();
                setSnackbarMessage('Процедура успешно добавлена');
            }
            else {
                return response.text().then(text => { throw new Error(text) })
            }            
        })
        .catch(error => {
            setSnackbarMessage('Не удалось добавить процедуру ' + error.message);
         });

        openSnackbar(true);
    };

    const Edit = async () => {

        if(!selectedData.service){
            return;
        }
        if(!selectedData.tooth){
            return;
        }
        if(!selectedVisitProcedure?.id){
            return;
        }

        const request : ProcedureFromVisitEditRequest = {
            id: selectedVisitProcedure.id,
            service: selectedData.service,
            countOfThisProcedures: selectedData.countOfThisProcedures,
            sum: selectedData.service?.price * selectedData.countOfThisProcedures,
            tooth: selectedData.tooth
        }; 

        const options = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(request)
        };

        fetch('https://localhost:7126/api/v1/Visit/EditProcedureFromVisit/' + visit?.id, options)
        .then(async response => {
            if (response.ok) {
                reloadPage();
                setSnackbarMessage('Процедура успешно обновлена');
            }
            else {
                return response.text().then(text => { throw new Error(text) })
            }            
        })
        .catch(error => {
            setSnackbarMessage('Не удалось обновить процедуру ' + error.message);
         });

        openSnackbar(true);
    };

    const handleByNameInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { name, value } = event.target;        
        setSelectedData(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    const visitProcedureChange= (event: SyntheticEvent<Element, Event>, value: any) => {
        setSelectedData(prevState => ({
            ...prevState,
            service: value
        }));
    };

    const toothChange= (event: SyntheticEvent<Element, Event>, value: any) => {
        setSelectedData(prevState => ({
            ...prevState,
            tooth: value
        }));
    };

    function reloadPage() {
        setTimeChange(new Date());
        // setTimeout(() => {
        //     window.location.reload();
        // }, 700);
    }

    return {
        selectedVisitProcedure, setSelectedVisitProcedure,
        selectedData, setSelectedData,
        visit, setVisit,
        visitProcedures, setVisitProcedures,
        Create,
        Edit,
        Delete,
        handleByNameInputChange,
        visitProcedureChange,
        toothChange
    }
}

export default VisitProcedureCRUD;