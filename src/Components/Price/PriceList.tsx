import { useState, useEffect } from "react";
import { Price } from "./Props";
import PriceCRUD from "./PriceCRUD";
import BaseValidaton from '../Base/BaseValidaton'; 
import BaseSnackbar from '../Base/BaseSnackbar'; 
import { Box, Button, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from "@mui/material";
import PriceModalWindow from "./PriceModalWindow";

export const PriceList = () => 
{
    //Переменные всплывающего окна, хранятся здесь, но при CRUD операциях устанавливаются сообщения
    const [snackbarOpen, setSnackbarOpen] = useState(false);
    const [snackbarMessage, setSnackbarMessage] = useState('');
    const [timeChange, setTimeChange] = useState(new Date());

    //CRUD функционал с ценами
    const 
    { 
        newPrice, setNewPrice,
        getAllPrice,
        CreatePrice, 
        EditPrice, 
        DeletePrice, 
        priceId, setPriceId,
        handleInputChange

    } = PriceCRUD(setSnackbarOpen, setSnackbarMessage,setTimeChange);

    const 
    { 
        validationSelectedRow

    } = BaseValidaton(setSnackbarOpen, setSnackbarMessage);
    
    
    const [tableData, setServicePriceList] = useState<Price[]>([]);

    const [showCreateWin, setCreateWin] = useState<boolean>(false);
    const [showEditWin, setEditWin] = useState<boolean>(false);
    const [showDeleteeWin, setDeleteWin] = useState<boolean>(false);

    const clickCreate = () =>
    {
        setNewPrice({});
        setPriceId(null);
        setCreateWin(true);  
    }

    const clickEdit = () =>
    {
        if(validationSelectedRow(priceId)){ 
            setEditWin(true);    
        }
    }

    const clickDelete = () =>
    {
        if(validationSelectedRow(priceId)){ 
            setDeleteWin(true); 
        }
    }

    useEffect(() => {
        const fetchData = async () => {
            const data = await getAllPrice();
            setServicePriceList(data);
        };
        fetchData();
    }, [timeChange]);

    //Нажатие на услугу из списка
    const handleRowClick = (id: string) => {
        setPriceId(id === priceId ? null : id);

        const selectedServicePrice = tableData.find(serviceprice => serviceprice.id === id);
        if (selectedServicePrice) {
            setNewPrice(selectedServicePrice);
        }
    };

    return (
        <Box display="flex" justifyContent="center">
            <div style={{ width: '80%' }}>
                <TableContainer component={Paper} style={{ width: '100%' }}>

                    <div style={{  marginLeft: '5rem', marginTop: '1rem', display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                        <h1>Услуги</h1>
                        <div style={{ marginBottom: '1rem', marginLeft: '1rem', marginTop: '1rem', marginRight: '4rem', textAlign: 'right' }}>
                            <Button variant="contained" color="primary" style={{ marginRight: '1rem' }} onClick={clickCreate}>Создать</Button>
                            <Button variant="contained" color="info" style={{ marginRight: '1rem' }} onClick={clickEdit}>Редактировать</Button>
                            <Button variant="contained" color="secondary" style={{ marginRight: '1rem' }} onClick={clickDelete}>Удалить</Button>
                        </div>
                    </div>

                    <Table aria-label='simple table'>
                        <TableHead>
                            <TableRow>
                                <TableCell><b>Код</b></TableCell>
                                <TableCell><b>Наименование</b></TableCell>
                                <TableCell><b>Цена</b></TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {tableData.map(row => (
                                <TableRow
                                    key={row.id}
                                    onClick={() => handleRowClick(row.id)}
                                    sx={{ cursor: 'pointer', backgroundColor: row.id === priceId ? '#91D1EC' : 'transparent' }}
                                >
                                    <TableCell>{row.code}</TableCell>
                                    <TableCell>{row.name}</TableCell>
                                    <TableCell>{row.price}</TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
                <PriceModalWindow
                    isOpen={showCreateWin}
                    buttonClose={() => {setCreateWin(false)}}
                    buttonAction={CreatePrice}
                    newData={newPrice}
                    handleInputChange={handleInputChange}
                    title={"Добавить позицию"}
                    titleButton={"Создать"}
                    disabled={false}
                />
                <PriceModalWindow
                    isOpen={showEditWin}
                    buttonClose={() => {setEditWin(false)}}
                    buttonAction={EditPrice}
                    newData={newPrice}
                    handleInputChange={handleInputChange}
                    title={"Редактирование позицию"}
                    titleButton={"Изменить"}
                    disabled={false}
                />
                <PriceModalWindow
                    isOpen={showDeleteeWin}
                    buttonClose={() => {setDeleteWin(false)}}
                    buttonAction={DeletePrice}
                    newData={newPrice}
                    handleInputChange={handleInputChange}
                    title={"Удаление позиции"}
                    titleButton={"Удалить"}
                    disabled={true}
                />
                <BaseSnackbar
                    isOpen={snackbarOpen}
                    message={snackbarMessage}
                    onClose={() => setSnackbarOpen(false)}
                />
            </div>
        </Box>
    );
}

export default PriceList;