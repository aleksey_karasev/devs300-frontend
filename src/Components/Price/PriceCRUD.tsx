import { useState } from "react";
import { Price } from './Props';

export const PriceCRUD = ( openSnackbar: (isOpen: boolean) => void, setSnackbarMessage: (message: string) => void, setTimeChange: (time:Date)=>void  ) => {

    const [priceId, setPriceId] = useState<string | null>(null);

    const [newPrice, setNewPrice] = useState<Partial<Price>>({});


    const CreatePrice = async () => {

        await messageHandlePrcie('Позиция успешно создана', 'Не удалось создать позицию', "https://localhost:7126/api/v1/ServicePrice", 'POST');
    };

    const EditPrice = async () => {
       
        await messageHandlePrcie('Позиция успешно изменена', 'Не удалось изменить позицию', "https://localhost:7126/api/v1/ServicePrice/", 'PUT', priceId);
    };

    const DeletePrice = async () => {
        
        await messageHandlePrcie('Позиция успешно удалена', 'Не удалось удалить позицию', "https://localhost:7126/api/v1/ServicePrice/", 'DELETE', priceId);
    };

    //После запроса вызывает сообщение о проделанной работе
    const messageHandlePrcie = async (messSeccess: string, messError: string, url: string, p_mehtod: string, id?:string | null) => {

        var response = await queryPrice(url, p_mehtod, priceId)

        if (response?.ok) {
            reloadPage();
            setSnackbarMessage(messSeccess); 
        } else {
            setSnackbarMessage(`${messError}: ${response?.statusText}`);
        }
    
        openSnackbar(true);
    }

    //Выполняет запрос
    const queryPrice = async (url: string, p_mehtod: string, id?:string | null) => {
        
        try 
        {            
            return await fetch(`${url}${id ?? ''}`, {
                method: p_mehtod,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer '+ localStorage.getItem('id_token')
                },
                body: JSON.stringify(newPrice)
            });

        } catch (error) {
            console.error("An error occurred:", error); 
        }
    };

    const getAllPrice = async () => 
    {
        const headers = { 'Authorization': 'Bearer '+ localStorage.getItem('id_token') };
        const response = await fetch("https://localhost:7126/api/v1/ServicePrice", { headers });
        return await response.json();
    }

    const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { name, value } = event.target;
        setNewPrice(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    function reloadPage() {
        setTimeChange(new Date());
        // setTimeout(() => {
        //     window.location.reload();
        // }, 700); 
    }

    return {
        newPrice, setNewPrice,
        getAllPrice,
        CreatePrice, 
        EditPrice, 
        DeletePrice, 
        priceId, setPriceId,
        handleInputChange
    }
}

export default PriceCRUD;