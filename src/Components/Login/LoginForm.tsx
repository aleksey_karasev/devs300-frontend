import axios, { AxiosResponse, AxiosRequestConfig, RawAxiosRequestHeaders } from 'axios';
import { Component, useState } from "react";
import './LoginForm.scss'
import { useNavigate } from 'react-router-dom';
import { Box, Button, Grid, TextField, Typography } from '@mui/material';
import { connect, useDispatch } from "react-redux";

interface LoginFormState {    
    login: string;
    password: string;
}

const client = axios.create({
  //baseURL: 'https://localhost:7126/api/v1/',
  baseURL: 'https://localhost:7071/'
});





// Синхронная функция логина
function login(login:string, password:string) {
  const config: AxiosRequestConfig = {
    headers: {
      'Accept': 'application/json',      
    } as RawAxiosRequestHeaders,
  };
  const data = {'login': login, 'password':password}
  console.log(data);   
  client.post('/Auth',data,config)
        .then((response)=>{

          console.log(response.status);
          console.log(response.data);
        })
        .catch((error)=>{
          console.log(error);
          alert('error login on backend' + error);
        });
};

function parseJwt (token: string) {
  var base64Url = token.split('.')[1];
  var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
  var jsonPayload = decodeURIComponent(window.atob(base64).split('').map(function(c) {
      return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
  }).join(''));

  return JSON.parse(jsonPayload);
}

const LoginForm = () => {
   
    const [login, setLogin] = useState('');
    const [password, setPassword] = useState('');
    const [role, setRole] = useState('');
    const [personId, setPersonId] = useState('');
    // Асинхронная функция логина
async function loginAsync(login:string,password:string)  { 
  const config: AxiosRequestConfig = {
    headers: {
      'Accept': 'application/json',      
    } as RawAxiosRequestHeaders,
  };    
  
  try {
    const data = {'email': login, 'password': password, 'role': role, 'personId': personId};    
    //const response: AxiosResponse = await client.post(`Auth/Login`, data , config);       
    const response: AxiosResponse = await client.post(`auth/Login`, data , config);       
    console.log('ответ: '+response.data.accessToken);   
    const parsedJwt=parseJwt(response.data.accessToken);//['role']);
   
    console.log("диспатчим is_authorized = true");
    dispatch({ type: '[BTN_EV] CHANGE_AUTH', payload:true });
    localStorage.setItem('id_token', response.data.accessToken);   
    axios.defaults.headers.common = {'Authorization': `Bearer ${response.data.accessToken}`} 
    dispatch({ type: '[BTN_EV] CHANGE_USER', payload:login });
    
    //dispatch({ type: '[BTN_EV] CHANGE_ROLE', payload:response.data.roles });
    dispatch({ type: '[BTN_EV] CHANGE_ROLE', payload:parsedJwt['role'] });
    //dispatch({ type: '[BTN_EV] CHANGE_PERSONID', payload:response.data.personId });
    dispatch({ type: '[BTN_EV] CHANGE_PERSONID', payload:parsedJwt['id'] });
    //dispatch({ type: '[BTN_EV] CHANGE_PERSONFIO', payload:response.data.personFullName });
    dispatch({ type: '[BTN_EV] CHANGE_PERSONFIO', payload: parsedJwt['name'] });
    navigate('/');      
    //alert('Успешно залогинен на сервере под логином '+login); 
   
  } catch(err) {
    if (axios.isAxiosError(err)) {
        if(err.response!=null)
            alert((err.response.data));
        else if (err.code==='ERR_NETWORK')
            alert('Сервер недоступен');
        else
            alert('Непонятная ошибка');            
      } else {
        alert('handleUnexpectedError('+err+')');
      }  
  } 
  
};
  //    const [state, setState] = useState<State>({
  //   onlineAppointments: null,
  //   loading: true,
  //   error: null,
  // });
    

     
      
      // return <div className="container">
      //           <div className="form">
      //             <div className="login-form">
      //               <input type="text" name="username" placeholder="Username" onChange={this.setLogin}/>
      //               <input type="password" name="password" placeholder="Password" onChange={this.setPassword}/>                    
      //               <button className="btn" onClick={() => 
      //                                                     { 
      //                                                       loginAsync(this.state.login,this.state.password); 
      //                                                       //navigate('/'); 
      //                                                     }}>Login</button>             
      //             </div>
      //           </div>
      //         </div>       
      const dispatch = useDispatch();
      const navigate = useNavigate(); 
      
      return <div className="container">
          <Grid container direction="column" alignItems="center">
            <Grid item>
              <Typography variant="h4" className="formTitle">
                Логин
              </Typography>
            </Grid>
            <Grid item>
              <Box className="formContainer">
                <form>
                  <Grid container direction="column" spacing={2}>
                    <Grid item>
                      <TextField
                        label="Логин"
                        variant="outlined"
                        fullWidth
                        className="formInput"
                        value = {login}
                        onChange={(e)=>setLogin(e.target.value)}
                      />                  
                    </Grid>
                    <Grid item>
                      <TextField
                        label="Пароль"
                        variant="outlined"
                        fullWidth
                        className="formInput"
                        type="password"                        
                        value={password}
                        onChange={(e)=>setPassword(e.target.value)}
                      />
                    </Grid>
                    <Grid item>
                      <Button variant="contained" className="formButton" fullWidth onClick={
                                                                                            (e)=>{ 
                                                                                                    loginAsync(login,password);
                                                                                                                                                                                              
                                                                                                 }}>
                        Войти
                      </Button>
                    </Grid>
                  </Grid>
                </form>
              </Box>
            </Grid>
          </Grid>
        </div>      
};

export default LoginForm;

