import { Button, Stack, Typography } from "@mui/material";
import { ScheduleProps } from "./Props";
import OneDaySchedule from "./OneDaySchedule";
import { GetSchedule } from "../../Services/ScheduleServices/scheduleService";
import { useEffect, useState } from "react";
import { MainReducerProps } from "../../StateManagement/mainreducer";
import { connect, useDispatch } from "react-redux";
import withAuthentication from "../WithAuthentication/WithAuthentication";
import { useNavigate } from "react-router-dom";


interface ScheduleEnterProps {
    isAuthenticated: boolean;
    beginSchedule?: string;
    endSchedule?: string;
}

const Schedule = (props: ScheduleEnterProps)=>{   
    const [data, setData] = useState<ScheduleProps>();
    const [isLoading, setLoading] = useState(false);
    const [beginSchedule, setBeginSchedule] = useState<string | undefined>();
    const [endSchedule, setEndSchedule] = useState();   

    const dispatch = useDispatch();
    const navigate = useNavigate();

    let begin = props.beginSchedule;
    if (props.beginSchedule===undefined){
        let date = new Date();    
        let day = date.getDate();   
        let month = date.getMonth() + 1;
        let year = date.getFullYear();

        // This arrangement can be altered based on how we want the date's format to appear.
        begin = `${year}-${month}-${day}`;
        
        dispatch({ type: '[BTN_EV] CHANGE_BEGIN_SCHEDULE', payload:begin });      
    } 

    useEffect(() => {        
        async function getData() {  
    
            if(begin!==undefined){         
                setLoading(true);
                const resp = await GetSchedule(begin);  
                setLoading(false);
                if(resp) setData(resp);       
            }     
        }
        getData();
     }, []);

    if (isLoading) return <div>Loading...</div>
  
    return <Stack  alignItems="center" spacing={2} direction="column" sx={{
        display: 'flex',
        justifyContent: 'center',
        p: 1,
        m: 1,
        bgcolor: 'background.paper',
        borderRadius: 1,
      }}>
       <Stack direction='row' spacing='2rem'> 
       <Button variant="contained" color="primary">Предыдущая неделя</Button>
        <Typography variant='h4'  component="div" sx={{ flexGrow: 1 }}><div>{data?.periodName}</div></Typography>     
       <Button variant="contained" color="primary" onClick={()=> {
                                                                    const bg = props.beginSchedule;
                                                                    if(bg!==undefined){
                                                                        let newDate = new Date(bg);
    
                                                                        // Добавляем один день
                                                                        newDate.setDate(newDate.getDate() + 7);
                                                                       
                                                                        let day = newDate.getDate();   
                                                                        let month = newDate.getMonth() + 1;
                                                                        let year = newDate.getFullYear();
                                                            
                                                                        // This arrangement can be altered based on how we want the date's format to appear.
                                                                        let stt = `${year}-${month}-${day}`;
                                                                       // alert(begin);
                                                                        dispatch({ type: '[BTN_EV] CHANGE_BEGIN_SCHEDULE', payload:stt });  
                                                                       
                                                                        navigate('/schedule');
                                                                    }
                                                                } 
                                                            }>Следующая неделя</Button>         
       </Stack>
       {data?.days?.map((oneDay)=> (        
          <OneDaySchedule date={oneDay.date} visits={oneDay.visits} key={oneDay.date}/>
       ))} 
    </Stack>
}
const mapReduxStateToProps = (globalReduxState: any): MainReducerProps => {
    console.log('globalredux',globalReduxState);
    return {          
        username: globalReduxState.namesStore.username, 
        isAuthenticated: globalReduxState.namesStore.isAuthenticated,
        personId: globalReduxState.namesStore.personId,
        role: globalReduxState.namesStore.role,
        personFullname: globalReduxState.namesStore.personFullname,
        isNewVisit: globalReduxState.namesStore.isNewVisit,
        dateTimeVisit: globalReduxState.namesStore.dateTimeVisit,
        visitId: globalReduxState.namesStore.visitId,
        beginSchedule: globalReduxState.namesStore.beginSchedule
    };
  }
  
export default connect(mapReduxStateToProps)(withAuthentication(Schedule));