export interface FullVisit {
    id: string
    dateTimeBegin: string
    dateTimeEnd: string
    dateOfVisit: any
    periodSimple: string
    dateVisit: string
    doctor: Doctor
    patient: Patient
    status: number
    comment: any
    fromOnline: boolean
    procedures: Procedure[]
  }
  
  export interface Doctor {
    id: string
    firstName: string
    fatherName: string
    lastName: string
    phoneNumber: string
    createdAt: string
    qualification: string
  }

  export interface EditVisitProps {
    username: string;
    isAuthenticated: boolean;
    isNewVisit: boolean;    
    visitId?: string;
    dateTimeVisit?: string;
    role: string;
    personId?: string;
    personFullname?: string;
  }
  
  export interface Patient {
    id: string
    firstName: string
    fatherName: string
    lastName: string
    phoneNumber: string
    createdAt: string
    allergy: string
  }
  
  export interface Procedure {
    id: string
    service: Service
    countOfThisProcedures: number
    sum: number
  }
  
  export interface Service {
    id: string
    code: string
    name: string
    price: number
  }
  