import { Box, Button, Paper, Stack, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from '@mui/material';
import './OneDaySchedule.scss'
import { OneDayProps, Visit } from './Props';
import { useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';

const OneDaySchedule = (props: OneDayProps) => {     
  const navigate = useNavigate()
  const dispatch = useDispatch();
  return  <Box display="flex" justifyContent="center">
  <Stack style={{ width: '100%' }} spacing='1rem'>
      <TableContainer component={Paper} style={{ width: '100%' }}>

          <div style={{  marginLeft: '5rem', marginTop: '1rem', display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
              <h1>{props.date}</h1>              
          </div>

          <Table aria-label='simple table'>
              <TableHead>
                  <TableRow>
                      <TableCell><b>Время</b></TableCell>
                      <TableCell><b>Врач</b></TableCell>
                      <TableCell><b>Пациент</b></TableCell>
                      <TableCell><b>Примечание</b></TableCell>
                      <TableCell><b></b></TableCell>
                  </TableRow>
              </TableHead>
              <TableBody>
              {props.visits.map(visit => (
                      <TableRow
                          key={visit.timePeriod}  
                          // className={'oneDayRow '+ (visit.patient!=null ? 'rowReserved' : 'rowUnreserved')}
                          >
                          <TableCell>{visit.timePeriod}</TableCell>
                          <TableCell>{visit.doctor}</TableCell>
                          <TableCell>{visit.patient}</TableCell>
                          <TableCell>{visit.comment}</TableCell>
                          <TableCell>
                            <Button onClick={() => {  
                                                     navigate(`/visits/`+visit.visitId, { state: { id: visit.visitId } }); 
                                                     dispatch({ type: '[BTN_EV] CHANGE_DATETIMEVISIT', payload:props.date });
                                                     dispatch({ type: '[BTN_EV] CHANGE_ISNEWVISIT', payload:false });
                                                     dispatch({ type: '[BTN_EV] CHANGE_VISITID', payload:visit.visitId });
                                                   }
                                            }>Изменить
                            </Button>
                           </TableCell>
                      </TableRow>
                  ))}
              </TableBody>
          </Table>
      </TableContainer>
      <Stack alignItems='flex-start'>
        <Button variant='contained' color='primary' onClick={() => {  
                                                                    navigate(`/visits/`+'0', { state: { id:'0' } }); 
                                                                    dispatch({ type: '[BTN_EV] CHANGE_DATETIMEVISIT', payload:props.date });
                                                                    dispatch({ type: '[BTN_EV] CHANGE_ISNEWVISIT', payload:true });
                                                                    dispatch({ type: '[BTN_EV] CHANGE_VISITID', payload:0 });
                                                                   }
                                                            }>Добавить</Button>
      </Stack>
  </Stack>
</Box>
    // return <>
    //     <table className="oneDayTable">  
    //       <caption>
    //         {props.date}
    //       </caption>    
    //       <thead>
    //           <tr>
    //             <th>
    //               Время
    //             </th>
    //             <th>
    //               Врач
    //             </th>
    //             <th>
    //               Пациент
    //             </th>
    //             <th>
    //               Примечание
    //             </th>
    //           </tr>      
    //           </thead>
    //           <tbody>
    //         {props.visits.map((visit) => (
    //             <tr className={'oneDayRow '+ (visit.patient!=null ? 'rowReserved' : 'rowUnreserved')} key={visit.timePeriod}>
    //                 <td className="oneDayCell">{visit.timePeriod}</td>
    //                 <td className="oneDayCell">{visit.doctor}</td>
    //                 <td className="oneDayCell">{visit.patient}</td>
    //                 <td className="oneDayCell">{visit.comment}</td>
    //                 <td><Button onClick={() => {  navigate(`/visits/`+visit.visitId); }}>Изменить</Button></td>
    //             </tr>          
    //     ))}
    //     </tbody>              
    //     </table>
    // </>
}

export default OneDaySchedule;