import { Autocomplete, Button, Checkbox, FormControl, FormControlLabel, FormHelperText, Input, InputLabel, MenuItem, Select, Snackbar, Stack, TextField } from "@mui/material"
import { useQuery } from "@tanstack/react-query";
import { GetVisit } from "../../Services/ScheduleServices/visitService";
import { useNavigate, useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import { EditVisitProps, FullVisit } from "./VisitProps";
import './EditVisit.scss'
import { DoctorShortDto, PatientShortDto, PeriodDto, UpdatedVisit } from "./Props";
import { GetAvailableTimes, GetDoctors } from "../../Services/ScheduleServices/getDoctorsService";
import { GetPatients } from "../../Services/ScheduleServices/getPatientsService";
import VisitProcedureTable from "../VisitProcedure/VisitProcedureTable";
import { MainReducerProps } from "../../StateManagement/mainreducer";
import { connect } from "react-redux";
import withAuthentication from "../WithAuthentication/WithAuthentication";


const EditVisit = (props: EditVisitProps)=>{   
  const navigate = useNavigate()
  const qp = useParams();   

  const visitId = qp["*"];  
  const [data, setData] = useState<FullVisit>();
  const [doctor,setDoctor] = useState<string>('');
  const [patient,setPatient] = useState<string>('');
  const [doctorError,setDoctorError] = useState('');
  const [isLoading, setLoading] = useState(false);
  const [period, setPeriod] = useState('');
  const [dateVisit, setDateVisit] = useState('');
  const [comment, setComment] = useState('');
  const [doctorId,setDoctorId] = useState<string | undefined>('');
  const [doctorDto, setDoctorDto] = useState<DoctorShortDto | undefined>();
  const [patientId,setPatientId] = useState<string | undefined>('');
  const [patientDto,setPatientDto] = useState<PatientShortDto | undefined>();
  const [fromOnline, setFromOnline] = useState<boolean>(false);
  const [doctors, setDoctors] = useState<DoctorShortDto[]>([]);
  const [patients, setPatients] = useState<PatientShortDto[]>([]);
  const [isNewPatient, setIsNewPatient] = useState<boolean>(false);
  const [patientPhone,setPatientPhone] = useState<string>('')
  const [patientFirstName,setPatientFirstName] = useState<string>('')
  const [patientSecondName,setPatientSecondName] = useState<string>('')
  const [patientLastName,setPatientLastName] = useState<string>('')  
  const [saveError,setSaveError] = useState<string>('')
  const [isNewVisit,setIsNewVisit] = useState<boolean>(false);
  const [periodDto,setPeriodDto] = useState<PeriodDto | undefined>();
  const [availableTimes, setAvailableTimes] = useState<PeriodDto[]>([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoading(true);
        const doctors = await GetDoctors();      
        if(doctors!=undefined){          
          setDoctors(doctors);
        }
        const patients = await GetPatients();
        if(patients!=undefined){
          const newPat: PatientShortDto = {
            id: '-1',
            description: '<Создать нового пациента>'
          }
          setPatients([newPat,...patients]);
        }
        
        if(!props.isNewVisit){          
          const response = await GetVisit(visitId);
          if(response){
            setData(response);
            if(response.doctor!=null){
                setDoctor(response.doctor.lastName+' '+response.doctor.firstName+' '+response.doctor.fatherName);
                setDoctorId(response.doctor.id);
                if(doctors!=undefined){
                  let d = doctors.find(p=>p.id===response.doctor.id);
                  setDoctorDto(d);
                }              
            } else {
                setDoctor('');
            }
            if(response.patient!=null) {
                setPatient(response.patient.lastName+' '+response.patient.firstName+' '+response.patient.fatherName);                
                setPatientId(response.patient.id);                
                if(patients!=undefined){             
                  let pp = patients.find(p=>p.id===response.patient.id);   
                  setPatientDto(pp);          
                }
            } else {
                setPatient('');
                setPatientId('');
            }

            setComment(response.comment??'');
            setFromOnline(response.fromOnline);

            setPeriod(response.periodSimple);
            setDateVisit(response.dateVisit);
          }
        } else {
          setDoctor('');
          const times = await GetAvailableTimes(props.dateTimeVisit??'');
          if(times!=undefined)
            setAvailableTimes(times);
          setIsNewVisit(true);
          setDateVisit(props.dateTimeVisit??'');
        }
      } catch (error) {
        console.error('Ошибка при получении данных:', error);
      }
      finally{
        setLoading(false);
      }
    };

    fetchData();
  }, []);

  const formatErrorMessages = (validationErrors:any) => {
    let messages = '';
    console.log(validationErrors['Comment']);
    for (const fieldName in validationErrors) {
      messages += `${fieldName}: ${validationErrors[fieldName].join(', ')}. `;
    }
    console.log(messages);
    return messages.trim();
  };

  const handleSave = async () => {
    // TODO: Отправка данных на сервер для сохранения профиля
    console.log('Сохранение визита...');
    // Пример кода для отправки данных на сервер:
    const updatedVisit: UpdatedVisit = {      
      id: props.isNewVisit?undefined: visitId,      
      dateVisit: dateVisit,
      simplePeriod: periodDto?.period,
      doctorId: doctorId===""?undefined:doctorId,
      patientId: patientId===""?undefined:patientId,
      comment: comment
    };
    if(isNewPatient){
        updatedVisit.newPatient = {
          firstName: patientFirstName,
          secondName: patientSecondName,
          lastName: patientLastName,
          phoneNumber: patientPhone
        }
    }

    if(patientId==='-2')
      updatedVisit.patientId='00000000-0000-0000-0000-000000000000';
    if(patientId==='-1')
      updatedVisit.patientId = undefined;
    const replacer = (key:any, value:any) =>
      typeof value === 'undefined' ? null : value;

    try {
      const response = await fetch('https://localhost:7126/api/v1/Visit/simple', {
        method: props.isNewVisit?'POST':'PUT',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(updatedVisit,replacer),
      });
    
      if (response.ok) {
        console.log('Профиль успешно сохранен!');
        navigate('/schedule');
      } else {
        console.log('какая-то ошибка!');
        // Если запрос вернул ошибку, обрабатываем её
        //const errorData = await response.text();
         //если ожидаем ошибку в виде json-формата       
        if (response.status === 400) {     
          let errorData = await response.json();
          if(errorData === undefined) {
            errorData = await response.text();     
            setSaveError(errorData);
          } else {
            const errorMessages = formatErrorMessages(errorData.errors);
            setSaveError(errorMessages);
          }
        } else {
          setSaveError('Ошибка сервера. Попробуйте повторить попытку позже.');
        }        
      }
    } catch (error) {
      // Обработка ошибок сети
      setSaveError('Ошибка сети. Попробуйте еще раз позже. ');
    }
    
    
  };

  function handleSave1(){
    if(doctor.length===0)
    {
      setDoctorError("Введите дохтура!")
      return;
    }

    alert('Сохранено!');
  }

  function handleCancel(){
    navigate('/schedule');
  }

  if (isLoading) {
    return <div>Загрузка...</div>;
  }
  const handleCloseError = () => {
    setSaveError('');
  };
   
    return <Stack direction="column" spacing={2} alignItems="center" style={{width: '100%'}} paddingTop='3rem' >
            <Stack direction="column" spacing={2} alignItems="center" >
            <TextField
                  required                
                  label="Дата"                  
                  value={dateVisit}
                  onChange={(event)=>setDateVisit(event.target.value)}
                  className="editVisitField"
                  variant="standard"
                  inputProps={{ readOnly: true}}
                  // inputProps={{ readOnly: !props.isNewVisit }}
              />         
              {!props.isNewVisit && <TextField
                  required                
                  label="Время"
                  value={period}
                  onChange={(event)=>setPeriod(event.target.value)}
                  className="editVisitField"
                  variant="standard"
                  inputProps={{ readOnly: true}}
              />    }
              {props.isNewVisit && <Autocomplete   
                className="editVisitField"           
                value={periodDto}
                onChange={(event: any, newValue: PeriodDto | null) => {               
                  if(newValue==null){
                    setPeriodDto(undefined);                    
                  }
                  else{
                    setPeriodDto(newValue);                         
                  }
                }}                      
                id="periodAutocomplete"  
                getOptionLabel={(option) => option.period}         
                options={availableTimes}
                renderInput={(params) => <TextField {...params} label="Время" />}
              />            }
              <Autocomplete   
                className="editVisitField"           
                value={doctorDto}
                onChange={(event: any, newValue: DoctorShortDto | null) => {               
                  if(newValue==null){
                    setDoctorDto(undefined);
                    setDoctorId(undefined);
                  }
                  else{
                    setDoctorDto(newValue);     
                    setDoctorId(newValue.id);                     
                  }
                }}                      
                id="doctorAutocomplete"  
                getOptionLabel={(option) => option.fio}         
                options={doctors}
                renderInput={(params) => <TextField {...params} label="Врач" />}
              />               
              <Autocomplete   
                className="editVisitField"           
                value={patientDto}
                onChange={(event: any, newValue: PatientShortDto | null) => {               
                  if(newValue===null)
                    setPatientDto(undefined);
                  else
                    setPatientDto(newValue);     
                  setPatientId(newValue?.id);      
                  if(newValue?.id==="-1")     
                    setIsNewPatient(true);
                  else
                    setIsNewPatient(false);
                }}                      
                id="patientAutocomplete"  
                getOptionLabel={(option) => { if (option!=undefined) return option.description; return ''; } }         
                options={patients}
                renderInput={(params) => <TextField {...params} label="Пациент" />}
              />
              
              {isNewPatient && <div className="newPatient">
                  <div>Новый пациент</div>
                  <Stack spacing={2} direction="column">
                    <TextField label="Фамилия" value={patientLastName} onChange={(e)=>setPatientLastName(e.target.value)} />
                    <TextField label="Имя" value={patientFirstName} onChange={(e)=>setPatientFirstName(e.target.value)} />
                    <TextField label="Отчество" value={patientSecondName} onChange={(e)=>setPatientSecondName(e.target.value)} />
                    <TextField label="Телефон" value={patientPhone} onChange={(e)=>setPatientPhone(e.target.value)}/>
                  </Stack>
                </div>}
              <TextField
                label="Примечание"
                value={comment}
                onChange={(event)=>setComment(event.target.value)}
                className="editVisitField"
              />   
              <FormControlLabel control={<Checkbox value={fromOnline} />} label="Из онлайн бронирования" disabled />            
              
              {/* <div>visitId = {visitId}</div> */}
              
              <Stack direction={"row"} spacing={2}>
                <Button variant="contained" onClick={handleSave} color="success">Сохранить</Button>
                <Button variant="contained" onClick={handleCancel} color="error">Отмена</Button>
              </Stack> 
            </Stack>            
            {!props.isNewVisit && <VisitProcedureTable></VisitProcedureTable>}
            {/* {visitId!=='0' && <VisitProcedureTable></VisitProcedureTable>} */}
            <Snackbar
              open={!!saveError}
              autoHideDuration={6000}
              onClose={handleCloseError}
              message={saveError}
              anchorOrigin={{ vertical:'bottom',horizontal:'center' }}
            />
    </Stack>
}
const mapReduxStateToProps = (globalReduxState: any): MainReducerProps => {
  console.log('globalredux',globalReduxState);
  return {          
      username: globalReduxState.namesStore.username, 
      isAuthenticated: globalReduxState.namesStore.isAuthenticated,
      personId: globalReduxState.namesStore.personId,
      role: globalReduxState.namesStore.role,
      personFullname: globalReduxState.namesStore.personFullname,
      isNewVisit: globalReduxState.namesStore.isNewVisit,
      dateTimeVisit: globalReduxState.namesStore.dateTimeVisit,
      visitId: globalReduxState.namesStore.visitId
  };
}

export default connect(mapReduxStateToProps)(withAuthentication(EditVisit));