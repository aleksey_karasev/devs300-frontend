export interface ScheduleProps {
    periodName?: string
    days?: OneDayProps[]
  } 

export interface OneDayProps {
    date: string
    visits: Visit[]
  }
  
export interface Visit {
    timePeriod: string
    patient?: string
    doctor?: string
    comment?: string
    visitId: string
}

export interface UpdatedVisit {
  id?: string
  doctorId?: string
  patientId?: string
  simplePeriod?: string
  dateVisit: string
  comment?: string
  newPatient?: newPatient
}

export interface newPatient {
  firstName?: string
  secondName?: string
  lastName?: string
  phoneNumber?: string
}
  
export interface DoctorShortDto {
  id?: string
  fio: string
  outerId?: string
}

export interface PatientShortDto {
  id?: string
  description: string
}

export interface PeriodDto {
  period: string
}