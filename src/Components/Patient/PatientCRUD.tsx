import { useState } from "react";
import { Patient } from './Props';
import { useNavigate } from "react-router-dom";

export const PatientCRUD = ( openSnackbar: (isOpen: boolean) => void, setSnackbarMessage: (message: string) => void,setTimeChange: (time:Date)=>void ) => {

    const navigate = useNavigate();

    const [patientId, setPatientId] = useState<string | null>(null);
    const [patientOuterId, setPatientOuterId] = useState<string | null>(null);

    const [newPatient, setNewPatient] = useState<Partial<Patient>>({});


    const CreatePatient = async () => {
        await messageHandlePatient('Пациент успешно создан', 'Не удалось создать пациента', "https://localhost:7071/api/patients/", 'POST');
        //await messageHandlePatient('Доктор успешно создан', 'Не удалось создать доктора', "https://localhost:7126/api/v1/Patient", 'POST');
        
    };

    const EditPatient = async () => {
        await messageHandlePatient('Пациент успешно изменен', 'Не удалось изменить пациента', "https://localhost:7071/api/patients/", 'PUT', patientOuterId);
        //await messageHandlePatient('Доктор успешно изменен', 'Не удалось изменить доктора', "https://localhost:7126/api/v1/Patient/", 'PUT', patientId);
    };

    const DeletePatient = async () => {
        await messageHandlePatient('Пациент успешно удален', 'Не удалось удалить пациента', "https://localhost:7071/api/patients/", 'DELETE', patientOuterId);
        //await messageHandlePatient('Доктор успешно удален', 'Не удалось удалить доктора', "https://localhost:7126/api/v1/Patient/", 'DELETE', patientId);
    };

    const toPatientCard = () =>{ 
        navigate('patientCard', {state:{id:newPatient.id}});
      }

    //После запроса вызывает сообщение о проделанной работе
    const messageHandlePatient = async (messSeccess: string, messError: string, url: string, p_mehtod: string, id?:string | null) => {
        console.log('messageHandlePatient');
        var response = await queryPatient(url, p_mehtod, patientOuterId??patientId)

        if (response?.ok) {
            reloadPage();
            console.log('setSnackbarMessage '+messSeccess);
            setSnackbarMessage(messSeccess); 
        } else {
            console.log('setSnackbarMessage ');
            setSnackbarMessage(`${messError}: ${response?.statusText}`);
        }
    
        openSnackbar(true);
    }

    //Выполняет запрос
    const queryPatient = async (url: string, p_mehtod: string, id?:string | null) => {
        
        try 
        {            
            return await fetch(`${url}${id ?? ''}`, {
                method: p_mehtod,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer '+localStorage.getItem('id_token')
                },
                body: JSON.stringify(newPatient)
            });

        } catch (error) {
            console.error("An error occurred:", error); 
        }

    };

    const getAllPatients = async () => 
    {
        const headers = { 'Authorization': 'Bearer '+localStorage.getItem('id_token') };
        const response = await fetch("https://localhost:7126/api/v1/Patient", { headers });
        return await response.json();
    }


    const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { name, value } = event.target;
        setNewPatient(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    function reloadPage() {
        setTimeChange(new Date());
        // setTimeout(() => {
        //     window.location.reload();
        // }, 700); 
    }

    return {
        newPatient, setNewPatient,
        getAllPatients,
        CreatePatient, 
        EditPatient, 
        DeletePatient, 
        patientId, setPatientId,
        patientOuterId, setPatientOuterId,
        handleInputChange,
        toPatientCard
    }
}

export default PatientCRUD;