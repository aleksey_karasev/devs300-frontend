import { PatientFormModalProps} from './Props';

import { Button, Box, Modal, TextField } from "@mui/material"

export const PatientModalWindow: React.FC<PatientFormModalProps> = ({ isOpen,  buttonClose, buttonAction, newData, handleInputChange, title, titleButton, disabled }) => {
    
    return <Modal
        open={isOpen}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
    >
        <Box
            sx={{
                position: 'absolute',
                top: '50%',
                left: '50%',
                transform: 'translate(-50%, -50%)',
                width: 400,
                bgcolor: 'background.paper',
                boxShadow: 24,
                p: 4,
            }}
        >
            <h2 id="modal-modal-title">{title}</h2>
            <TextField 
                label="Имя"
                name="firstName"
                value={newData.firstName || ''}
                onChange={handleInputChange}
                fullWidth
                margin="normal"
                disabled={disabled}
            />
            <TextField
                label="Фамилия"
                name="lastName"
                value={newData.lastName || ''}
                onChange={handleInputChange}
                fullWidth
                margin="normal"
                disabled={disabled}
            />
            <TextField
                label="Отчество"
                name="fatherName"
                value={newData.fatherName || ''}
                onChange={handleInputChange}
                fullWidth
                margin="normal"
                disabled={disabled}
            />
            <TextField
                label="Номер телефона"
                name="phoneNumber"
                value={newData.phoneNumber || ''}
                onChange={handleInputChange}
                fullWidth
                margin="normal"
                disabled={disabled}
            />
            <TextField
                label="Эл. почта"
                name="email"
                value={newData.email || ''}
                onChange={handleInputChange}
                fullWidth
                margin="normal"
                disabled={disabled}
            />
            <TextField
                label="Алергия"
                name="allergy"
                value={newData.allergy || ''}
                onChange={handleInputChange}
                fullWidth
                margin="normal"
                disabled={disabled}
            />
            <Box mt={2} display="flex" justifyContent="flex-end">
                <Button variant="contained" color="primary" onClick={buttonAction}>{titleButton}</Button>
                <Button variant="contained" color="secondary" onClick={buttonClose} style={{ marginLeft: '10px' }}>Закрыть</Button>
            </Box>
        </Box>
    </Modal>
};


export default PatientModalWindow;