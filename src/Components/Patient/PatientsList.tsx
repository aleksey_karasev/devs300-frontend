import { useState, useEffect } from "react";

import { Patient } from './Props';
import { PatientModalWindow } from './PatientModalWindow';
import PatientCRUD from './PatientCRUD'; 
import BaseValidaton from '../Base/BaseValidaton'; 
import BaseSnackbar from '../Base/BaseSnackbar'; 

import {TableContainer, Table, TableHead, TableBody, TableRow, TableCell, Paper, Button, Box } from "@mui/material"

  
export const PatientsList = () => 
{
    //Переменные всплывающего окна, хранятся здесь, но при CRUD операциях устанавливаются сообщения
    const [snackbarOpen, setSnackbarOpen] = useState(false);
    const [snackbarMessage, setSnackbarMessage] = useState('');
    const [timeChange, setTimeChange] = useState(new Date());

    //CRUD функционал с доктором
    const 
    { 
        newPatient, setNewPatient,
        getAllPatients,
        CreatePatient, 
        EditPatient, 
        DeletePatient, 
        patientId, setPatientId,
        patientOuterId, setPatientOuterId,
        handleInputChange,
        toPatientCard

    } = PatientCRUD(setSnackbarOpen, setSnackbarMessage,setTimeChange);

    const 
    { 
        validationSelectedRow

    } = BaseValidaton(setSnackbarOpen, setSnackbarMessage);
    
    
    const [tableData, setPatientsList] = useState<Patient[]>([]);

    const [showCreateWin, setCreateWin] = useState<boolean>(false);
    const [showEditWin, setEditWin] = useState<boolean>(false);
    const [showDeleteeWin, setDeleteWin] = useState<boolean>(false);

    const clickCreate = () =>
    {
        setNewPatient({});
        setPatientId(null);
        setCreateWin(true);  
    }

    const clickEdit = () =>
    {
        if(validationSelectedRow(patientId)){ 
            setEditWin(true);    
        }
    }

    const clickDelete = () =>
    {
        if(validationSelectedRow(patientId)){ 
            setDeleteWin(true); 
        }
    }

    useEffect(() => {
        const fetchData = async () => {
            const data = await getAllPatients();
            setPatientsList(data);
        };
        fetchData();
    }, [snackbarMessage]);

    //Нажатие на доктора из списка
    const handleRowClick = (id: string) => {
        setPatientId(id === patientId ? null : id);

        const selectedPatient = tableData.find(patient => patient.id === id);
        if (selectedPatient) {
            setPatientOuterId(selectedPatient.outerId);
            setNewPatient(selectedPatient);
        }
    };

    return (
        <Box display="flex" justifyContent="center">
            <div style={{ width: '80%' }}>
                <TableContainer component={Paper} style={{ width: '100%' }}>

                    <div style={{  marginLeft: '5rem', marginTop: '1rem', display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                        <h1>Пациенты</h1>
                        <div style={{ marginBottom: '1rem', marginLeft: '1rem', marginTop: '1rem', marginRight: '4rem', textAlign: 'right' }}>
                            <Button variant="contained" color="primary" style={{ marginRight: '1rem' }} onClick={clickCreate}>Создать</Button>
                            <Button variant="contained" color="info" style={{ marginRight: '1rem' }} onClick={clickEdit}>Редактировать</Button>
                            <Button variant="contained" color="secondary" style={{ marginRight: '1rem' }} onClick={clickDelete}>Удалить</Button>
                            <Button variant="contained" color="primary" style={{ marginRight: '1rem' }} onClick={toPatientCard} disabled={!patientId}>Просмотр карты</Button>
                        </div>
                    </div>

                    <Table aria-label='simple table'>
                        <TableHead>
                            <TableRow>
                                <TableCell><b>Пациент</b></TableCell>                                
                                <TableCell><b>Номер телефона</b></TableCell>
                                <TableCell><b>Аллергия</b></TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {tableData.map(row => (
                                <TableRow
                                    key={row.id}
                                    onClick={() => handleRowClick(row.id)}
                                    sx={{ cursor: 'pointer', backgroundColor: row.id === patientId ? '#91D1EC' : 'transparent' }}
                                >
                                    <TableCell>{row.fatherName} {row.firstName} {row.lastName}</TableCell>
                                    <TableCell>{row.phoneNumber}</TableCell>
                                    <TableCell>{row.allergy}</TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
                <PatientModalWindow
                    isOpen={showCreateWin}
                    buttonClose={() => {setCreateWin(false)}}
                    buttonAction={CreatePatient}
                    newData={newPatient}
                    handleInputChange={handleInputChange}
                    title={"Создание нового пациента"}
                    titleButton={"Создать"}
                    disabled={false}
                />
                <PatientModalWindow
                    isOpen={showEditWin}
                    buttonClose={() => {setEditWin(false)}}
                    buttonAction={EditPatient}
                    newData={newPatient}
                    handleInputChange={handleInputChange}
                    title={"Редактирование пациента"}
                    titleButton={"Изменить"}
                    disabled={false}
                />
                <PatientModalWindow
                    isOpen={showDeleteeWin}
                    buttonClose={() => {setDeleteWin(false)}}
                    buttonAction={DeletePatient}
                    newData={newPatient}
                    handleInputChange={handleInputChange}
                    title={"Удаление врача. Вы действительно хотите удалить пациента?"}
                    titleButton={"Удалить"}
                    disabled={true}
                />
                <BaseSnackbar
                    isOpen={snackbarOpen}
                    message={snackbarMessage}
                    onClose={() => setSnackbarOpen(false)}
                />
            </div>
        </Box>
    );
}

export default PatientsList;