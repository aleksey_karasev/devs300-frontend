
export interface Patient {
  id: string;
  firstName: string;
  fatherName: string;
  lastName: string;
  phoneNumber: string;
  email: string;
  allergy: string;
  outerId: string;
}

export interface PatientFormModalProps {
  isOpen: boolean;
  buttonClose: () => void;
  buttonAction: () => void;
  newData: Partial<Patient>;
  handleInputChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  title: string;
  titleButton: string;
  disabled: boolean;
}

