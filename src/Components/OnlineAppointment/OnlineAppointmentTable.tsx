import { useState, useEffect } from "react";
import { Box, Button, Grid } from '@mui/material';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { useNavigate } from 'react-router-dom';
import { OnlineAppointment } from './Props';
import dayjs from 'dayjs';
import { ThemeProvider } from '@mui/material/styles';
import { createTheme } from '@mui/material/styles';
import { grey } from '@mui/material/colors';
import ApproveOnlineAppointmentWindow from "./ApproveOnlineAppointmentWindow";
import OnlineAppointmentCRUD from "./OnlineAppointmentCRUD";
import OnlineAppointmentSnackbar from "./OnlineAppointmentSnackbar";


interface State {
  onlineAppointments: OnlineAppointment[] | null;
  loading: boolean;
  error: string | null;
}

const useRows = (): State => {
  const [state, setState] = useState<State>({
    onlineAppointments: null,
    loading: true,
    error: null,
  });

  const navigate = useNavigate();  

  useEffect(() => {
    const fetchRows = async () => {
      try {
        const headers = { 'Authorization': 'Bearer ' + localStorage.getItem('id_token') };
        const response = await fetch("https://localhost:7126/api/v1/OnlineAppointment", { headers });
        if (response.ok) {
          const onlineAppointments = await response.json();
          const ops = onlineAppointments.data;
          setState({ onlineAppointments:ops, loading: false, error: null });
        } else {
          if (response.status === 401) {
           // alert('Пользователь не авторизован!');
            navigate('/login');
          }
          console.log(response.status);
        }
      } catch (error) {
        console.log('catched error: ' + error);
      }
    };

    fetchRows();
  }, []);

  return state;
};

function formatDateTime(value: string | null | undefined) {
  if (!value) {
    return value;
  }
  return dayjs(Date.parse(value)).format("DD.MM.YYYY HH:MM");
}

const theme = createTheme({
  components: {
    MuiTableCell: {
      styleOverrides: {
        root: {
          borderRight: `1px solid`,
          borderTop: `1px solid`,
          borderColor: grey[300]
        },
      },
    },

    // MuiTableRow: {
    //   styleOverrides: {
    //     root: {
    //       '&:hover': {
    //         backgroundColor: orange[100]
    //       },
    //     }
    //   }
    // },

    MuiTable: {
      styleOverrides: {
        root: {
          spacing: 10
        },
      },
    }
  },
});

export default function OnlineAppointmentTable() {
  const { onlineAppointments, loading, error } = useRows();

  const [isOpenEditorWindow, setIsOpenEditorWindow] = useState<boolean>(false);

  const [snackbarOpen, setSnackbarOpen] = useState(false);
  const [snackbarMessage, setSnackbarMessage] = useState('');
  

  const
    {
      selectedOnlineAppointment, setSelectedOnlineAppointment,
      result, setResult,
      Approve,
      Delete,
      handleByNameInputChange,
      handleByIdInputChange

    } = OnlineAppointmentCRUD(setSnackbarOpen, setSnackbarMessage);

  if (loading) {
    return <div>Loading...</div>;
  }

  if (error) {
    return <div>{error}</div>;
  }

  const handleRowClick = (onlineAppointment: OnlineAppointment) => {    
    if (onlineAppointment == selectedOnlineAppointment) {
      setSelectedOnlineAppointment(null);
    }
    else {
      setSelectedOnlineAppointment(onlineAppointment);
      setSelectedOnlineAppointment(onlineAppointment);
      setResult({
        phoneNumber: onlineAppointment.phoneNumber,
        firstName: "",
        fatherName: "",
        lastName: "",
        slotTime: null,
        comment: onlineAppointment.description
      });
    }
  };

  return (

    <Box display="flex" justifyContent="center" marginTop='5rem'>
      <div style={{ width: '80%' }}>
        <ThemeProvider theme={theme}>
          <TableContainer component={Paper}>

            <div style={{ marginLeft: '5rem', marginTop: '1rem', display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
              <h1>Online записи</h1>
              <div style={{ marginBottom: '1rem', marginLeft: '1rem', marginTop: '1rem', marginRight: '4rem', textAlign: 'right' }}>
                <Button variant="contained"
                  style={{ marginRight: '1rem', minWidth: '150px', maxWidth: '150px' }}
                  onClick={() => { setIsOpenEditorWindow(true) }}
                  disabled={!selectedOnlineAppointment}>Подтвердить</Button>
                <Button variant="contained"
                  style={{ marginRight: '1rem', minWidth: '150px', maxWidth: '150px' }}
                  onClick={Delete}
                  disabled={!selectedOnlineAppointment}>Удалить</Button>
              </div>
            </div>

            <Table>
              <TableHead>
                <TableRow sx={{ cursor: 'pointer', backgroundColor: 'transparent' }}>
                  <TableCell align="center"><b>Телефон</b></TableCell>
                  <TableCell align="center"><b>Почта</b></TableCell>
                  <TableCell align="center"><b>Желаемая дата</b></TableCell>
                  <TableCell align="center"><b>Описание</b></TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {onlineAppointments?.map((onlineAppointment) => (
                  <TableRow key={onlineAppointment.id}
                    onClick={() => handleRowClick(onlineAppointment)}
                    sx={{ cursor: 'pointer', backgroundColor: onlineAppointment.id === selectedOnlineAppointment?.id ? '#91D1EC' : 'transparent' }}>
                    <TableCell align="right">{onlineAppointment.phoneNumber}</TableCell>
                    <TableCell align="right">{onlineAppointment.email}</TableCell>
                    <TableCell align="right">{formatDateTime(onlineAppointment.wantedDateVisit)}</TableCell>
                    <TableCell align="right">{onlineAppointment.description}</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </ThemeProvider>

        <ApproveOnlineAppointmentWindow
          isOpen={isOpenEditorWindow}
          result={result}
          buttonClose={() => { setIsOpenEditorWindow(false) }}
          buttonApprove={Approve}
          handleByNameInputChange={handleByNameInputChange}
          handleByIdInputChange={handleByIdInputChange}
          disabled={false}
        />
        <OnlineAppointmentSnackbar
          isOpen={snackbarOpen}
          message={snackbarMessage}
          onClose={() => setSnackbarOpen(false)}
        />
      </div>
    </Box>
  );
}