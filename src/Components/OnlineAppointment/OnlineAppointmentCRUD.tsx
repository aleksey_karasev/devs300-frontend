import { SyntheticEvent, useState } from "react";
import { OnlineAppointment, OnlineAppointmentApprove, SlotTime } from './Props';

export const OnlineAppointmentCRUD = (openSnackbar: (isOpen: boolean) => void, setSnackbarMessage: (message: string) => void) => {

    const [selectedOnlineAppointment, setSelectedOnlineAppointment] = useState<OnlineAppointment | null>(null);
    const [result, setResult] = useState<OnlineAppointmentApprove>({phoneNumber:"", 
    firstName: "",
    fatherName: "",
    lastName: "",
    slotTime: null,      
    comment: ""});


    const Approve = async () => {    

        const request = {
            visitId: result.slotTime?.visitId,
            firstName: result.firstName,
            lastName: result.lastName,
            fatherName: result.fatherName,
            phoneNumber: result.phoneNumber,
            comment: result.comment
        };

        const requestOptions = {
            method: 'PUT',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(request)
        };

        fetch('https://localhost:7126/api/v1/OnlineAppointment/' + selectedOnlineAppointment?.id, requestOptions)
        .then(async response => {
            if (response.ok) {
                reloadPage();
                setSnackbarMessage('Online запись успешно подтверждена');
            }
            else {
                return response.text().then(text => { throw new Error(text) })
            }            
        })
        .catch(error => {
            setSnackbarMessage('Не удалось подтвердить online запись ' + error.message);
         });

        openSnackbar(true);
    };

    const Delete = async () => {
        const url = `https://localhost:7126/api/v1/OnlineAppointment/${selectedOnlineAppointment?.id}`;
        fetch(url, {
            method: 'DELETE',
            headers: { 'Content-Type': 'application/json' },
        })
        .then(async response => {
            if (response.ok) {
               // reloadPage();
                setSnackbarMessage('Online запись успешно удалена');
            }
            else {
                return response.text().then(text => { throw new Error(text) })
            }         
        }).catch(error => {
            setSnackbarMessage('Не удалось удалить online запись ' + error);
         });

         openSnackbar(true);
    };

    const handleByNameInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { name, value } = event.target;

        console.log(`handleInputChange ${name}: ${value}`);

        setResult(prevState => ({
            ...prevState,
            [name]: value
        }));

        console.log(result);
    };

    const handleByIdInputChange = (event: SyntheticEvent<Element, Event>, value: any) => {       
        setResult(prevState => ({
            ...prevState,
            slotTime: value
        }));

        console.log(`handleByIdInputChange ${result.slotTime}`);
    };

    function reloadPage() {
        // setTimeout(() => {
        //     window.location.reload();
        // }, 700);
    }

    return {
        selectedOnlineAppointment, setSelectedOnlineAppointment,
        result, setResult,
        Approve,
        Delete,
        handleByNameInputChange,
        handleByIdInputChange
    }
}

export default OnlineAppointmentCRUD;