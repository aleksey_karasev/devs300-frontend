import { OnlineAppointmentProps, SlotTime } from './Props';
import { useState, useEffect } from "react";
import { Button, Box, Modal, TextField, Autocomplete, Grid, Stack } from "@mui/material"

export const ApproveOnlineAppointmentWindow: React.FC<OnlineAppointmentProps> = ({ isOpen, result, buttonClose, buttonApprove, handleByNameInputChange, handleByIdInputChange, disabled }) => {

    const [slotTimes, setSlotTimes] = useState<SlotTime[]>([]);
    const [error, setError] = useState(false);

    useEffect(() => {
        const fetchRows = async () => {
            const response = await fetch("https://localhost:7126/api/v1/Visit/times");
            const times = await response.json();
            setSlotTimes(times);
        };
        fetchRows();
    }, []);


    return <Modal
        open={isOpen}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
    >
        <Box
            sx={{
                position: 'absolute',
                top: '50%',
                left: '50%',
                transform: 'translate(-50%, -50%)',
                width: 400,
                bgcolor: 'background.paper',
                boxShadow: 24,
                p: 4,
            }}
        >
            <h2 id="modal-modal-title">Подтверждение online записи</h2>
            <TextField
                name="phoneNumber"
                label="Телефон"
                value={result?.phoneNumber || ''}
                onChange={handleByNameInputChange}
                fullWidth
                margin="normal"
                disabled={true}
            />
            <TextField
                name="firstName"
                required
                label="Имя"
                error={!!error}
                value={result?.firstName || ''}
                onChange={handleByNameInputChange}
                fullWidth
                margin="normal"
                disabled={disabled}
            />
            <TextField
                name="lastName"
                required
                label="Фамилия"
                error={!!error}
                value={result?.lastName || ''}
                onChange={handleByNameInputChange}
                fullWidth
                margin="normal"
                disabled={disabled}
            />
            <TextField
                name="fatherName"
                required
                label="Отчество"
                error={!!error}
                value={result?.fatherName || ''}
                onChange={handleByNameInputChange}
                fullWidth
                margin="normal"
                disabled={disabled}
            />

            <Autocomplete
                id="slotTime"
                fullWidth
                options={slotTimes}
                value={result?.slotTime}
                getOptionLabel={(slotTime) => slotTime.slotTime}
                onChange={(e, newValue) => { handleByIdInputChange(e, newValue); }}
                renderInput={(params) => <TextField {...params} label="Доступные слоты" required />}
                disabled={disabled} />

            <TextField
                name="comment"
                label="Комментарий"
                value={result?.comment || ''}
                onChange={handleByNameInputChange}
                fullWidth
                margin="normal"
                disabled={disabled}
            />
            <Box mt={2} display="flex" justifyContent="flex-end">
                <Button variant="contained" onClick={() => {
                    setError(false);
                    if(!result.firstName || !result.lastName || !result.lastName){
                        setError(true);
                    }
                    else{
                        buttonApprove(); 
                    }                    
                    }}>Подтвердить</Button>
                <Button variant="contained" onClick={buttonClose} style={{ marginLeft: '10px' }}>Закрыть</Button>
            </Box>
        </Box>
    </Modal>
};


export default ApproveOnlineAppointmentWindow;