
import { Snackbar } from "@mui/material";
import { SnackbarProps}  from "./Props";
import  { useEffect, useState } from "react";

const OnlineAppointmentSnackbar = ({ isOpen, message, onClose }: SnackbarProps) => {

    const [snackbarOpen, setSnackbarOpen] = useState(isOpen);

    useEffect(() => {
        setSnackbarOpen(isOpen);
    }, [isOpen]);

    useEffect(() => {

        if (isOpen) {
            
            const timer = setTimeout(() => {
                onClose(); 
            }, 10000);
            
            return () => clearTimeout(timer);
        }

    }, [isOpen, onClose]);
    
    return <Snackbar
        open={snackbarOpen}
        autoHideDuration={10000}
        onClose={onClose}
        message={message}
    />

};

export default OnlineAppointmentSnackbar;