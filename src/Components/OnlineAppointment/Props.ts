import { SyntheticEvent } from "react";

export interface OnlineAppointment {
    id: string;
    phoneNumber: string;
    wantedDateVisit: string;
    description: string;
    email: string;
    approved: boolean;
  }

  export interface Doctor {
    id: string;
    firstName: string;
    fatherName: string;
    lastName: string;
    phoneNumber: string;
    createdAt: string;
    qualification: string;
  }

  export interface OnlineAppointmentProps {
    isOpen: boolean;
    result: OnlineAppointmentApprove;
    buttonClose: () => void;
    buttonApprove: () => void;
    handleByNameInputChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
    handleByIdInputChange: (event: SyntheticEvent<Element, Event>, value: any) => void;
    disabled: boolean;
  }

  export interface OnlineAppointmentApprove {
    slotTime: SlotTime | null;
    firstName: string;
    fatherName: string;
    lastName: string;
    phoneNumber: string;
    comment: string;
  }

  export interface OnlineAppointmentApproveRequest {
    visitId: string;
    firstName: string;
    fatherName: string;
    lastName: string;
    phoneNumber: string;
    comment: string;
  }

  export interface SlotTime {
    slotTime: string;
    visitId: string;
  }

  export interface SnackbarProps {
    isOpen: boolean;
    message: string;
    onClose: () => void;
  }