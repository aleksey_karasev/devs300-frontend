import { Box, Button, Paper, Stack, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, ThemeProvider, createTheme } from "@mui/material";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { MainReducerProps } from "../../StateManagement/mainreducer";
import { connect } from "react-redux";
import { GetVisitsByDoctorId } from "../../Services/ScheduleServices/scheduleService";
import { VisitForDoctor } from "../VisitProcedure/Props";
import { grey } from "@mui/material/colors";


interface VisitForDoctorProps {
  username: string;
  isAuthenticated: boolean; 
  role: string;
  personId?: string;
  personFullname?: string;
}

const theme = createTheme({
    components: {
      MuiTableCell: {
        styleOverrides: {
          root: {
            borderRight: `1px solid`,
            borderTop: `1px solid`,
            borderColor: grey[300]
          },
        },
      },
  
      // MuiTableRow: {
      //   styleOverrides: {
      //     root: {
      //       '&:hover': {
      //         backgroundColor: orange[100]
      //       },
      //     }
      //   }
      // },
  
      MuiTable: {
        styleOverrides: {
          root: {
            spacing: 10
          },
        },
      }
    },
  });

const VisitsForDoctor = (props:VisitForDoctorProps) =>{
    const [data, setData] = useState<VisitForDoctor[]>([]);
    const [isLoading, setLoading] = useState(false);
    const [selectedVisit, setSelectedVisit] = useState<VisitForDoctor | null>(null);
    useEffect(() => {        
        async function getData() {            
            setLoading(true);
            const resp = await GetVisitsByDoctorId(props.personId??'');  
            setLoading(false);
            if(resp) setData(resp);           
        }
        getData();
     }, []);

    const handleRowClick = (visit: VisitForDoctor) => {    
    if (visit == selectedVisit) {
        setSelectedVisit(null);
    }
    else {
        setSelectedVisit(visit);    
     }
    };

    const navigate = useNavigate();
    return <ThemeProvider theme={theme}>
            <Box display="flex" justifyContent="center">
            <Stack direction={"column"}>
                <h1>Посещения на сегодня</h1>
                <TableContainer component={Paper}>
                    <Table aria-label='simple table'>                
                    <TableHead>
                        <TableRow>
                            <TableCell><b>Время</b></TableCell>                        
                            <TableCell><b>Пациент</b></TableCell>
                            <TableCell><b>Статус</b></TableCell>
                            <TableCell><b></b></TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {data.map(visit => (
                            <TableRow
                                key={visit.timePeriod}  
                                onClick={()=>handleRowClick(visit)}
                                sx={{ cursor: 'pointer', backgroundColor: visit.visitId === selectedVisit?.visitId ? '#91D1EC' : 'transparent' }}                                
                                // className={'oneDayRow '+ (visit.patient!=null ? 'rowReserved' : 'rowUnreserved')}
                                >
                                <TableCell>{visit.timePeriod}</TableCell>                            
                                <TableCell>{visit.patient}</TableCell>
                                <TableCell>{visit.status}</TableCell>
                                <TableCell><Button onClick={() => {  navigate(`/visits/`+visit.visitId, { state: { id: visit.visitId } }); }}>Редактировать</Button></TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                    </Table>
                </TableContainer>
            </Stack>
           </Box>
           </ThemeProvider>
}
const mapReduxStateToProps = (globalReduxState: any): MainReducerProps => {
  console.log('globalredux',globalReduxState);
  return {          
      username: globalReduxState.namesStore.username, 
      isAuthenticated: globalReduxState.namesStore.isAuthenticated,
      personId: globalReduxState.namesStore.personId,
      role: globalReduxState.namesStore.role,
      personFullname: globalReduxState.namesStore.personFullname,
      isNewVisit: globalReduxState.namesStore.isNewVisit,
      dateTimeVisit: globalReduxState.namesStore.dateTimeVisit,
      visitId: globalReduxState.namesStore.visitId
  };
}
  
export default connect(mapReduxStateToProps)(VisitsForDoctor);