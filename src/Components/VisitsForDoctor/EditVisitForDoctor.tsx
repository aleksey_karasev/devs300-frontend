import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { GetFullVisit } from '../../Services/ScheduleServices/visitService';
import { FullVisit } from '../Schedule/VisitProps';
import { Box, Button, Paper, Stack, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Theme, ThemeProvider, Typography, createStyles, createTheme, makeStyles } from '@mui/material';
import { grey } from '@mui/material/colors';

interface Procedure {
    name: string;
    quantity: number;
    price: number;
}

const theme = createTheme({
    components: {
        MuiTableCell: {
            styleOverrides: {
                root: {
                    borderRight: `1px solid`,
                    borderTop: `1px solid`,
                    borderColor: grey[300]
                },
            },
        },

        MuiTable: {
            styleOverrides: {
                root: {
                    spacing: 10
                },
            },
        }
    },
});

const EditVisitForDoctor = () => {    
    const qp = useParams();   
    const [data, setData] = useState<FullVisit|undefined>();

    const visitId = qp["*"];

    useEffect(() => {        
        async function getData() {                        
            const resp:FullVisit | undefined = await GetFullVisit(visitId);  
            
            if(resp) setData(resp);           
        }
        getData();
     }, []);    

    return (
        <Box display="flex" justifyContent="center" marginTop="3rem">
        <Paper  elevation={3}>
            <ThemeProvider theme={theme}>
            <Stack display="flex" justifyContent="center" direction="column" spacing='1rem' padding='1rem'>
                <Stack direction='column' spacing='1rem'>
                    <Typography variant="h5">Время приёма: {data?.periodSimple}</Typography>
                    <Typography variant="h5">Врач: {data?.doctor.lastName+' '+data?.doctor.firstName+' '+data?.doctor.fatherName}</Typography>
                    <Typography variant="h5">Пациент: {data?.patient.lastName+' '+data?.patient.firstName+' '+data?.patient.fatherName}</Typography>                     
                </Stack>
                <TableContainer component={Paper}>
                    <Table stickyHeader aria-label="procedures table">
                        <TableHead>
                            <TableRow>
                                <TableCell><b>Название процедуры</b></TableCell>
                                <TableCell><b>Количество</b></TableCell>
                                <TableCell><b>Цена</b></TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {data?.procedures.map((procedure, index) => (
                                <TableRow key={index}>
                                    <TableCell>{procedure.service.name}</TableCell>
                                    <TableCell>{procedure.countOfThisProcedures}</TableCell>
                                    <TableCell>{procedure.sum}</TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>   
                
                
                
                <Stack direction="row" padding="1rem" spacing='1rem' display='flex' justifyContent='space-between'>
                   <Stack direction='row' spacing='1rem'>
                        <Button variant='contained' color='primary'>Добавить</Button>
                        <Button variant='contained' color='warning'>Изменить</Button>
                        <Button variant='contained' color='error'>Удалить</Button>
                    </Stack>
                    <Typography variant="h5" display='flex' justifyContent='end'>Сумма: { data?.procedures.reduce((accumulator, object) => {  return accumulator + object.sum;}, 0)}</Typography>                                 
                </Stack>                                
                <Box display='flex' justifyContent='end'>
                    <Button variant='contained' color='success'>Завершить приём</Button>
                </Box>
                </Stack >
            </ThemeProvider>
        </Paper>
        </Box>
    );
};
export default EditVisitForDoctor;